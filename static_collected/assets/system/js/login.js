$("form").submit(function (e) {
     $("button[type=submit]").prop("disabled",true);
     e.preventDefault();
    w=$(this).serialize();
    $.ajax({
    url:"/login/",
    type: 'POST',
    data: w,
    success: function () {
        unblock_form();
        $('#msmlogin').removeClass('hide');
        $('#msmlogin').addClass('alert alert-danger');
        $("#msmlogin").html("Sesión Iniciada, Redirigiendo...");
        location.reload();
    },
    error: function (resp) {
        if (resp.status === 0) {
            $('#msmlogin').removeClass('hide');
            $('#msmlogin').addClass('alert alert-warning');
            $('#msmlogin').empty().prepend('Not connect: Verify Network.');
          } else if (resp.status === 400) {
            unblock_form();
            var errors = JSON.parse(resp.responseText);
            console.log(errors);
            for (ee in errors) {
                if (ee === '__all__'){
                    $('#msmlogin').removeClass('hide');
                    $('#msmlogin').addClass('alert alert-danger');
                    $('#msmlogin').empty().prepend(errors[ee][0].message);
                    $("button[type=submit]").prop("disabled",true);
                }
                var id = '#id_' + ee;
                $(id).after(errors[ee].message);
            }

          }else{
            $('#msmlogin').removeClass('hide');
            $('#msmlogin').addClass('alert alert-warning');
            $('#msmlogin').empty().prepend('Not connect: Error'+ resp.status );
        }
        console.log(resp);
    },
    beforeSend: function (xhr, status) {
                // TODO: show spinner
               // $("#algo").attr('style', 'position: absolute;height: 100%; width: 100%; z-index: 100;');
               // $('#spinner').fadeIn(400);
                for (i=1; i<=100; i++){
                    $("#pace-bar").css('width', i + '%');
                }

            },


    });
});
$("input").keydown(function () {
    $("button[type=submit]").prop("disabled",false);
    $("#pace-bar").css('width', 1 + '%');
    $("#msmlogin").empty();
    $('#msmlogin').addClass('hide');
});
function unblock_form() {
   $('.errorlist').remove();
}


