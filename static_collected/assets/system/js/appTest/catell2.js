
function initAnswer(p) {
     $("#modal_test").css("display",'block');
     $("#pasar").prop("disabled",true);

    $.getJSON('test_psyco/test/json_answer?test=catell2').done( function(data){
        //t = _.template($("#t_testaf5").html());
       // $("#result_test").html(t());
        contenido=data;
        console.log(data);
        test = data[0]["test"];
        data = data[0]["preguntas"];
        d = data[p-1];
        mensaje="no termine";
        if(d["correlativo"]==contenido[0]["total"]) {
            mensaje="ya termine";
        }else{
            mensaje="no termine";
        }
        t = data.length;
        $("#porcentaje_test").html(p+"/"+t).css('width',(110*p/t)+"%");
        $("#pregunta_test").html("PREGUNTA "+p+": "+d["descripcion"]);
        inp =  '<div class="row">';
        if (["test1","test3"].includes(d["dimensiones"])){
            inp += '<div class="col s11">';
            if ("test1"==d["dimensiones"]){
                wd = "350px"
            }else{
                wd="150px"
            }
            inp +=  '<img src="/static/media/'+d["imagenes"][0]["None-None"][0]+'" class="responsive-img" width="'+wd+'"">';
            inp += '</div>';
            inp += '<div class="col s1">';
            inp += '<div id="droppable" align="center" class="ui-widget-header">';
            inp += '<p>Arrastar Aquí</p>';
            inp += '</div>';
            inp += '</div>';

            inp += '<h4 align="center">Alternativas</h4>';
            inp += '<div class="col s12" align="center" style="margin-left: 20%">';
            for (i in d["alternativas"]){
                alt = d["alternativas"][i];
                //inp += '<div class="dra ui-widget-content" style="background: url(/static/media/'+alt["imagen"]+') no-repeat; background-size: 100px 100px;" >';
                //inp += '<p>'+alt["descripcion"]+','+alt["id_alt"]+'</p>';
                //inp += '</div>';
            }
            inp += '</div>';
        }
        else if ("test4"==d["dimensiones"]){
            wd="100px";
            inp += '<h4 align="center">Seleccione la alternativa: </h4>';
            inp +=  '<img src="/static/media/'+d["imagenes"][0]["None-None"][0]+'" class="responsive-img" width="'+wd+'"">';
        }
        else{
            inp += '<h4 align="center">Marque la imagen que no guarde relación</h4>';
        }
        inp += '<div id="alternativascatel" class="col s12" align="center" style="margin-left: 20%">';
            for (i in d["alternativas"]){
                alt = d["alternativas"][i];
                inp += '<div class="dra" align="center" onclick="alternativaTest(this,\''+alt["descripcion"]+'\','+alt["id_alt"]+')">';
                inp +=  '<img src="/static/media/'+alt["imagen"]+'" class="responsive-img" width="100px" heigth="100px">';
                inp += '<p style="color: black">'+alt["descripcion"]+'</p>';
                inp += '</div>';
            }
        inp += '</div>';

        inp += '</div>';

        inp += '<input type="hidden" name="valor" id="valor_test"/>';
        inp += '<input type="hidden" name="idpregunta" value="'+d["id"]+'" />';
        inp += '<input type="hidden" name="tiempot_test" id="tiempot_test" />';
        inp += '<input type="hidden" name="idtest" value="'+test+'" />';
        inp += '<input type="hidden" name="termino" value="'+mensaje+'" />';
        inp += '<input type="hidden" name="idalternativa" id="idalternativa"  />';

        if (p == t+1){
            inp += '<input type="hidden" name="bandera" value="yes" />';
            ct = ('fin_test()');
        }else {
            ct = ('start_test(' + (p + 1) + ');return false');
        }
        $("#alternativa_test").html(inp);
        $("#form_test").attr("onsubmit",ct);
        horaTest(d["tiempo_ejecucion"]);
        horaTrantest(200);
        $( ".dra" ).draggable({ revert: "invalid" });
        $("#droppable").droppable({
              classes: {
                "ui-droppable-active": "ui-state-active",
                "ui-droppable-hover": "ui-state-hover"
              },
           drop: function( event, ui ) {
                  txt = (ui.draggable.context.innerText).split(',');
                $("#valor_test").val(txt[0]);
               $("#idalternativa").val(txt[1]);
                $( ".dra" ).draggable( "destroy" )
           },

        });
    });
}

function alternativaTest(t, v,id) {
    $(".active").each(function () {
        $(this).removeClass('active');
    });
    $(t).addClass('active');
    $("#valor_test").val(v);
    $("#idalternativa").val(id);
}


function comenzar_test(p) {
    if (p==1){
        initAnswer(p);
        return false;
    }else{
        datocatell2=$("#form_test").serialize();
        idprogramacion=$('#idprogramacion').val();
        iddetprog=$('#iddetprogramacion').val();
        $.post("test_psyco/test/guardar_test/",datocatell2+'&idprogramacion='+idprogramacion+'&iddetpro='+iddetprog)
            .done(function (data) {
            initAnswer(p);
            noti("success","guardado");
            return false;
        }).error(function () {
            noti("error","no guardado");
        });
    }
    return false;
}

function cerrarModalTest(id) {
    l = Lobibox.confirm({
            msg : "¿Está seguro de cerrar el test sin antes terminar: ?",
            callback: function ($this, type, ev) {
                if(type=="yes"){
                    $(id).css("display",'none');
                }
            }
        });

}
function horaTest(c) {
    (function deleteDot() {
        setTimeout(function() {
            if (c-- > 0) {
              $("#timeTest").html("00:"+c);
              deleteDot();
            }
            if (c == 0){
                $("#pasar").prop('disabled',false);
            }
          }, 1000);
        })();
}

function horaTrantest(i) {
    c = 0;
    console.log("milton");
    if($("#validadortiempo").val()==""){
        $("#validadortiempo").val("1");
    (function deleteTime() {
        if (i-- > 0) {
            c++;
            $("#tiempot_test").val(c);
            setTimeout(deleteTime,1000);
            console.log(c);
        }
    })();}
}

