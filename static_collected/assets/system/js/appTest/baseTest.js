function initAnswer(p,test) {
     $("#modal_test").css("display",'block');
     $("#pasar").prop("disabled",true);
    if (!sessionStorage.getItem("test"+ test)) {
        $.getJSON('test_psyco/test/questions/?test=' + test).done(function (data) {
            sessionStorage.setItem("test"+test, JSON.stringify(data));
            build_test(p,data,test);
        })
    }else{
        build_test(p,JSON.parse(sessionStorage.getItem("test"+test)),test);
    }
}

function build_test(p,data,test) {
    contenido = data;
    d = data[p-1];
    tlen = data.length;
    if (p == tlen) {
        ct = ('start_test("fin");return false');
        mensaje = "ya termine";
    } else {
        mensaje = "no termine";
        ct = ('start_test(' + (p + 1)+',' +test+');return false');
    }
    $("#porcentaje_test").html(p + "/" + tlen).css('width', (110 * p / tlen) + "%");
    $("#pregunta_test").html(p + ": " + d["descripcion"]);
    inp = '<div class="col s12 m12 l12" style="padding-bottom: 0">';
    if (d['imagenes_pregunta']) {
        img = d['imagenes_pregunta'];
        for (im in img) {
            inp += '<img src="/static_collected/media/' + img[im] + '"   class="img-responsive img-test-alt" data-caption="' + img[im] + '">';
        }
    }
    inp+='<input type="hidden" name="tiempo_ejecucion" value="'+d["tiempo_ejecucion"]+'">';
    inp += "</div>";
    contador = 0;

    inp += '<div class="col s12 m12 l12" id="id_alternativa" >';
    $.each(d["alternativas"], function (key, value) {
        contador = contador + 1;
        if (value['alternativas__tipo__nombre'] == "escoger") {
            if(contador==1){
                inp += '<input type="hidden" id="marcado" value="" />';
            }

            if (value['alternativas__imagen']) {
                inp += '<div class="col s12 m4 l4 " >';
                inp += '<img class="materialboxed img-responsive img-test-alt"  onclick="img_test(' + value['id'] + ')" data-caption="' + value['alternativas__descripcion'] + '"  src="/static_collected/media/' + value['alternativas__imagen'] + '"/>';
            }
            inp += '<p style="float: left">';
            puntaje = ((value['puntaje']) ? value['puntaje'] : value['alternativas__puntajeAsignado']);
            inp += '<input type="radio"  onclick="llenarvalor('+puntaje+')" class="radio-inline" id="idalternativa' + contador + '" name="answer" value="' + value['id'] + '">';
            inp += '<label style="color: #71124a;font-size: 16px;text-shadow: 0.1px 0.3px grey;" for="idalternativa' + contador + '">' + value['alternativas__descripcion'] + '</label>';
            //inp += '<input type="hidden" name="value_marked" value="'+value['puntaje']+'" />';
            inp += '</p>';
            if (value['alternativas__imagen']) {
                inp += '</div>'
            }
        } else if (value['alternativas__tipo__nombre'] == "escala") {
            inp += '<p class="range-field" >';
            inp += '<input type="hidden" name="answer" value="' + value['id'] + '">';
            inp += '<input type="range" id="range-t" onclick="enabledbtn(false)" name="value_marked" min="' + value['alternativas__escala']['min'] + '" max="' + value['alternativas__escala']['max'] + '" value="' + value['alternativas__escala']['max'] * 0.1 + '" />';
            inp += ' </p>';
        }

    });
    inp += '</div>';
    inp += '<input type="hidden" name="validadortiempo" id="validadortiempo" />';
    inp += '<input type="hidden" name="time_marked" id="time_marked" />';
    inp += '<input type="hidden" name="termino" value="' + mensaje + '" />';

    $("#alternativa_test").html(inp);
    $("#form_test").attr("onsubmit", ct);
    $('.materialboxed').materialbox();
    timeTest(d["tiempo_ejecucion"]);
    horaTrantest(7200,p);
}

function enabledbtn(b){
    //$("#pasar").prop('disabled',b);
}

var forwad_a = function() {
    a = function(){
        $.get("test_psyco/test/clear_answer", {'patient':$("[name=history]").val()});
    };

    window.onbeforeunload = function() {
      return a();
    };

    if (window.history && window.history.pushState) {

    window.history.pushState('forward', null);

    window.onpopstate = function(event) {
        l = Lobibox.confirm({
            showClass: 'fadeInDown',
            hideClass: 'fadeUpDown',
            buttons: {
                si: {
                    text: 'si',
                    closeOnClick: true
                },
                no: {
                    text: 'no',
                    closeOnClick: true
                }
            },
            title:false,
            msg : "¿Está seguro de cerrar el test sin antes terminar: ?",
            callback: function ($this, type, ev) {
                if(type==="si"){
                    $('#modal_test').css("display",'none');
                    window.onbeforeunload='';
                    $.get("test_psyco/test/clear_answer", {'patient':$("[name=history]").val()});

                    location.reload();

                }
            }
        });
    };

  }
};

function start_test(p, test) {

    $("#pasar").prop("disabled", true);
        if (p == 1) {
            initAnswer(p,test);
            forwad_a();
            return false;
        } else {
            typ = $('input[name=answer]').attr('type');

            if (typ=="hidden"){
                vali = true;
            }else {
                vali = $('input[name=answer]:checked').val() != undefined;
            }

            if (vali) {
                datoa = $("#form_test").serialize();
                param =  datoa ;

                if($("#marcado").prop("tagName")=='INPUT' && $("#marcado").attr('type')!='range'){
                    param=param+'&value_marked='+$("#marcado").val()
                }
                saveQuestion(param,test,p);
                }else{
                noti("error", "Elegir una Opción");
                $("#pasar").prop("disabled", false);
            }
        }

    return false;
}

function saveQuestion(param,test,p) {
    enabledbtn(true);
    var $f=$("#pasar");
    if ($f.data('locked') != "undefined" && !$f.data('locked')) {
        $f.data('locked', true);
        $.post("test_psyco/test/guardar_test/", param + '')
            .done(function (data) {
                 $f.data('locked', false);
                if (data === "Falta completar el tiempo de espera par marcar") {
                    $f.prop("disabled", false);
                    noti("error", data);

                } else {
                    if (p.toString() == "fin") {
                        noti("success", "test finalizado correctamente");
                        window.onbeforeunload='';
                        window.onpopstate = '';

                        $("#modal_test").css("display", 'none');
                        window.location.href = "#test_psyco/test/result/?hist=" + $("input[name=history]").val();
                    } else {
                        initAnswer(p, test);
                        noti("success", "guardado");
                    }
                }
                return false;
            }).error(function () {
            $f.data('locked', false);
            noti("error", "no guardado");
        });
    }
}

function closeModalTest(id) {
    l = Lobibox.confirm({
            showClass: 'fadeInDown',
            hideClass: 'fadeUpDown',
            buttons: {
                si: {
                    text: 'si',
                    closeOnClick: true
                },
                no: {
                    text: 'no',
                    closeOnClick: true
                }
            },
            title:false,
            msg : "¿Está seguro de cerrar el test sin antes terminar: ?",
            callback: function ($this, type, ev) {
                if(type==="si"){
                    $(id).css("display",'none');
                }
            }
        });

}
function timeTest(c) {
    typ = $('input[name=answer]').attr('type');
    if (typ == "hidden"){
        bo = false
    }else{
        bo = false
    }
    (function deleteDot() {
        setTimeout(function() {
            if (c-- > 0) {
              $("#timeTest").css('display','');
              $("#timeTest").html("0"+c);
              deleteDot();
            }
            if (c == 0){
                $("#timeTest").css('display','none');
                $("#pasar").prop('disabled',bo);
            }
          }, 1000);
        })();
}

function horaTrantest(i,p) {
    c = 0;
    if(p==1){
        if($("#validadortiempo").val()===""){
            $("#validadortiempo").val("1");
        (function deleteTime() {
            if (i-- > 0) {
                c++;
                $("#time_marked").val(c);
                setTimeout(deleteTime,1000);
            }
        })();}
    }
}

function llenarvalor(a){
    $("#marcado").val(a)
}

function img_test(id) {
    $("input[value="+id+"]").prop("checked",true);
}



