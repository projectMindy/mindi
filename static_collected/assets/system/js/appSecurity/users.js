$("#id_profiles").select2({
        placeholder: "Seleccione un Tipo de Usuario",
        allowClear: true,
    }
);

function atras() {
    window.history.back();
}

function reUser(u){
    $.post(u, form, function(data){
                unblock_form();
            }).fail(function(resp) {
            unblock_form();
            var errors = JSON.parse(resp.responseText);
            console.log(resp);
            for (e in errors) {
                var id = '#id_' + e;
                $(id).after(errors[e]);
            }
            }).done(function(){
                traemedatos('security/usuarios');
            });
    return false;
}
    function moreUser(id){
        $.get('security/usuarios/view/',{'id':id},function(d){
             $('#resultados').empty().html(d);
        });
    }

function formuser(t){
        formData = new FormData($(t)[0]);
        tt = $("button").text();
        tt = $.trim(tt);
        if (tt == "Registrar") {
            u = 'security/users/add/'
        }else{
            u = "security/usuarios/actualizarUsuario/"+parseInt($(t).attr('idd'))+"/"
        }
            $.ajax({
                url: u,
                type: 'POST',
                data: formData,
                //necesario para subir archivos via ajax
                cache: false,
                contentType: false,
                processData: false,

                success: function (data) {
                    unblock_form();
                    $(t).empty();
                    window.history.back();
                },
                error: function (resp) {
                    unblock_form();
                    var errors = JSON.parse(resp.responseText);
                    for (e in errors) {
                        var id = '#id_' + e;
                        $(id).after(errors[e]);
                    }
                }
            });
}



function unblock_form() {
        $('.errorlist').remove();
    }
