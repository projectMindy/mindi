    function addF(t,url,ids,tm,idm) {
        m =  Modal(idm,"AGREGAR "+url.split('/')[2].toUpperCase(),'<div class="row"></div>',"",tm);
        $('#myModal').html(m);
        $("#"+idm).modal();
        $("#"+idm).modal('open');
        $("#myModal .modal-body .row").load(url+" .row", function (d) {
            dd = $(d).find("form").attr("onsubmit","guardarF(this,'','"+ids+"','#"+idm+"'); return false");
            $("#myModal .modal-body .row").html(dd);
            $("#myModal select").select2({'width':'100%'});
            $('#myModal').attr('tabindex','');
        });
    }
    function guardarF(tf,n,a,ids,idm){
        d = $(tf).serialize();
        u = a+'/'+n+'/add/';
    l = Lobibox.confirm({
        msg : "¿Está seguro de guardar: ?",
        callback: function ($this, type, ev) {
            if(type=="yes"){
                $.post(u,d).done(function (da) {
                    noti('success', "ok");
                    da = JSON.parse(da);
                    $(ids).select2({width:"100%",
                      data: da
                    }).val(da[0].id).trigger("change");
                    $(idm).modal();
                    $(idm).modal('close');
                    return false;
                },'json').fail(function(r) {
                        $('.errorlist').remove();
                        var errors = JSON.parse(r.responseText);
                                for (e in errors) {
                                    var id = '#id_' + e;
                                    $(id).after(errors[e]);
                                }
                });
                return false;
            }
        }
    });

}

function todos_se(n) {
        a=[];
        $(n+" option").each(function(){
        a.push($(this).val())
        });
        ninguno_se(n);
        $(n).val(a).trigger("change")
}

function ninguno_se(n){
    $(n).val(null).trigger("change")
    return true;
}
function tt_se(n) {
    ids = capId(n,"select");
    for (var i=0; i<ids.length; i++){
        todos_se(ids[i]);
    };
}
function nn_se(n){
    ids = capId(n,"select");
    for (i=0; i<ids.length; i++){
        ninguno_se(ids[i]);
    }
    return true;
}
function ttt_se(n) {
    idt = capId(n,"table");
    for (var j=0; j<idt.length; j++){
       ids =  capId(idt[j],"select");
        for(var t=0;t<ids.length; t++){
            todos_se(ids[t]);
        }
    }
}
function nnn_se(n) {
    idt = capId(n,"table");
    for (var j=0; j<idt.length; j++){
       ids =  capId(idt[j],"select");
        for(var t=0;t<ids.length; t++){
            ninguno_se(ids[t]);
        }
    }
}
function capId(n,t) {
   a=[];
   $(n+" "+t).each(function(index){
      a.push("#"+$(this).attr("id"));
   });
    return a;
}

function change_url(n,id) {
        url = window.location.hash;
        url=url.split("?");
        url2 = url[0].substr(1, url[0].length);

        if (url.length>1){
            pa = url[1].split("&");
            if (pa.length<=1){
                no = pa[0].split("=");
                if(no[0] == n){
                    nuu = "";
                }else{
                    nuu = pa+"&"
                }
                nu = nuu+n+"="+id;
            }else{
                nu = "";

                bf = nb =false;
                for (i in pa){

                    if (pa[i].split("=")[0] == n){
                        if (i==pa.length-1){
                            bb=""; aa="&"
                        }else {
                            bb="&";
                            aa="";
                            if (i>0) {
                                aa = "&"
                            }
                        }
                        nu+=aa+n+"="+id+bb;
                        bf = true;
                        nb = bf;
                    }else{
                        if (nb){
                            cs ="";
                            nb=false;
                        }else if(i==0){
                            cs=""
                        }else{
                            cs = "&";
                        }
                        nu+=cs+pa[i];
                    }
                }
                if (bf==false){
                    nu+="&"+n+"="+id
                }
            }
        }else{
            nu = n+"="+id;
        }
        window.location.hash=url[0]+"?"+nu;
    }

function delete_maintenance(a, n, i){
    l = Lobibox.confirm({
        msg : "¿Está seguro de eliminar: ?",
        callback: function ($this, type, ev) {
            if(type=="yes"){
                $.get(a+'/'+n+'/delete/'+i+'/',function(data){
                    traemedatos(a+"/"+n+"/listar");
                    noti('error',data);
                });
            }
        }
    });
}



function save_maintenance(t, id, n, a){
    $("#btn_envio").attr("disabled","true");

    formData = new FormData($(t)[0]);
    if (id){
        url = a+'/'+n+'/update/'+id+'/';
    }else{
        url = a+'/'+n+'/add/';
    }
    error = true;
    if ($("div").hasClass("m2m")) {
        ar = [];
        $("tbody tr").each(function (i, k) {
            v = $(this).find("select, textarea, input").serializeArray();
            //v = formData;
            s = '{';
            error = true;
            for (i in v) {
                formData.delete(v[i]['name']);
                if (v[i]['value'].trim() == "") {
                    noti("error", "falta completar " + v[i]['name']);
                    error = false;
                } else {
                    s += '\'' + v[i]['name'] + '\':';
                    s += '\'' + (v[i]['value']).trim() + '\',';
                    rea = v[0]['value'];
                }

            }
            s += '}';
            ar.push(s);
        });
        formData.append('m2m',ar);
        console.log(ar);
    }
    if (error) {
        l = Lobibox.confirm({
            title:'guardado',
            msg: "¿Está seguro de guardar: ?",
            callback: function ($this, type, ev) {
                if (type == "yes") {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: formData,
                        cache: false,
                        contentType: false,
                        processData: false,

                        success: function (da) {
                            noti('success', da);
                            atras();
                            return false;
                        },
                        error: function (r) {
                            $('.errorlist').remove();
                            var errors = JSON.parse(r.responseText);
                            for (e in errors) {
                                var id = '#id_' + e;
                                $(id).after(errors[e]);
                            }
                            return false;
                        }

                    });
                    return false;
                }
            }
        });
    }
    return false;
}

    function add_row() {
        filas = $('tbody tr').length;
        a = [];
        $("select").select2("destroy");
        $("tbody label").each(function (t) {
            tr = $(this).attr('for');
            a.push(tr);
        });
        ts = "test"+(filas+1);
        while ($.inArray(ts,a)>0){
            filas = filas + 1;
            ts = "test"+(filas);
        }
        f = $('tbody tr:nth-child(1)').clone();
        f.find("label").attr("for",ts);

        f.find("input[type=checkbox]").attr("id",ts);
        f.find("input[type=hidden]").remove();
        f.find('#del_row').css('display','');
        $('tbody').append(f);
        $('select').select2({width: '100%'});
    }

    function delete_row(t) {
        filas = $('tbody tr').length;
        if (filas != 1){
            $(t).closest('tr').remove();
        }else{
            noti('error','No permitido');
        }
    }