function crear_crud(app,name) {
    $.get('security/modules/crear_crud',{'name':name,'app':app}).done(function (d) {
        noti("success",d);
    });
}
function SeeSubMod(p,d) {
    $.getJSON('security/modules/vista_modulo/?id='+p+'&type=json', function(data) {
        a = '<a class="btn btn-primary pointer"  onclick = "addSubModForm('+p+',\''+d+'\')">agregar</a>';
       if (data != ''){
           t = _.template(a+$("#tsubmodule").html());
           $("#sub").empty().html(t({data:data,d:d,p:p}));
       }else{
        $("#sub").empty().html(a);
       }
    });
}
function addSubModForm(id,de){
    m =  Modal('idM',"AGREGAR SUB MÓDULO",'',"<input onclick = 'addSubModSave("+id+",\""+de+"\")' type='button' value ='registrar' class='btn btn-primary'>","");
    $('#myModal').html(m);
    $('#idM').modal();
    $('#idM').modal('open');
    t = _.template($("#tFormSubM").html());
    $(".modal-body").empty().html(t({data:null,id:id,de:de}));
    se(".moduleA");
    i =1;
    $('#idM').attr('tabindex','');
    $('span[dir="ltr"]').each(function() {
        i++;
        if (i == 4){
          $(this).remove();
        }

    });
}
function addSubModSave(p,d){
    f = $(".modal-body #formSubM").serialize();
    $.post('security/submodules/savesudmodulo/',f, function(data) {
       SeeSubMod(p,d);
       noti('success',data);
    }).fail(function(resp) {
        errornormal(resp,"formSubM");
    }).done(function(){
        cerrarModal('idM');
    });
}
//------------editar y eliminar modules hijos------------
function updateSubM(id,de,p){
    $.getJSON("security/modules/editmh","idmh="+id,function(data){
        $.each(data, function(key,value){
           m =  Modal('idM',"ACTUALIZAR SUB MÓDULO",'',"<input onclick = 'updateSaveSubMod("+id+",\""+de+"\","+p+")' type='button' value ='registrar' class='btn btn-primary'>","");
           $('#myModal').html(m);
           $('#idM').modal();
           $('#idM').modal('open');

            //captura evento de selecion en select2
           t = _.template($("#tFormSubM").html());
           $(".modal-body").empty().html(t({data:data,id:p,de:de}));

           se(".moduleA");
           $('#formSubM .select2-selection .select2-selection__rendered').append(value.icon__html_class);
           $(".moduleA").on("select2:select",function(e){ $('#formSubM .select2-selection .select2-selection__rendered').empty().append( e.params.data.clase); });
           $('#idM').attr('tabindex','');
           i =1;
            $('span[dir="ltr"]').each(function() {
                i++;
                if (i == 4){
                  $(this).remove();
                }
            });
        });
    });

}

function updateSaveSubMod(id,d,p){
  f = $("#formSubM").serialize();
  $.post('security/modules/edit/'+id+'/', f+'&mod', function(data) {
        SeeSubMod(p,d);
        noti("success",data)
  }).fail(function(resp) {
        errornormal(resp,"formSubM");
        noti("error",'falta llenar datos')
  }).done(function(a){
        cerrarModal('idM');
  });
}

function deleteSubMod(id,d,p){
    $.get('security/modules/delete/'+id+'/', function(data) {
        SeeSubMod(p,d);
        noti("error",data);
    });
}

function kdes(t,u){
    $(".modal-body #url").val(u+"/"+$(t).val());
}