from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('apps.security.urls')),
    path('', include('apps.adm_security.urls')),
    path('', include('apps.adm_institucion.urls')),
    path('', include('apps.test_psyco.urls')),
    path('', include('apps.adm_test.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
