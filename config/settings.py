import os
import configparser

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
from typing import List

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'h@hoz68*@u$l1goq9x3jua4l!)^bfe+fjspi%59^-ym3wmd5$2'

# SECURITY WARNING: don't run with debug turned on in production!
ALLOWED_HOSTS = ['*']
config = configparser.RawConfigParser()
config.read(BASE_DIR+'/config.ini')
DEPLOY = int(config['DEPLOYMENT']['production'])
DEBUG = int(config['DEPLOYMENT']['debug'])
dbName = config['DEPLOYMENT']['dbname']
# Application definition

DJANGO_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

THIRD_PARTY_APPS = ('',)

LOCAL_APPS = (
    'apps.security',
    'apps.adm_security',
    'apps.adm_institucion',
    'apps.test_psyco',
    'apps.adm_test',
)

INSTALLED_APPS = DJANGO_APPS + LOCAL_APPS  # THIRD_PARTY_APPS

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'apps.security.core.MiddlewareFunciones.MiddelwareValidarInstitucion'  # validar Institución
]
if DEPLOY:
    MIDDLEWARE.insert(1, 'whitenoise.middleware.WhiteNoiseMiddleware')

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',  # para los archivos media
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

def capitalize_keys(d):
    result = {}
    for key, value in d.items():
        upper_key = key.upper()
        result[upper_key] = value
    return result


if DEPLOY:
    base = dict(config['DB_PRODUCTION_'+str(dbName)])
else:
    base = dict(config['DB'])
    base = capitalize_keys(base)

DATABASES = {
    'default': base
}

env = ['local', 'production']


# if DEPLOY:
#     import dj_database_url
#
#     prod_db = dj_database_url.config(conn_max_age=500)
#     DATABASES['default'].update(prod_db)
#     # Honor the 'X-Forwarded-Proto' header for request.is_secure()
#     SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
#
#     ALLOWED_HOSTS = ['.herokuapp.com']
#
#     #  Add configuration for static files storage using whitenoise
#     STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
#     PROJECT_ROOT = os.path.join(os.path.abspath(__file__))

# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'es-pe'

LC_TIME = "es_ES.UTF-8"

LC_ALL = "es_Es"

USE_I18N = True

TIME_ZONE = 'America/Lima'

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/


STATIC_URL = '/static_collected/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]
STATIC_ROOT = os.path.join(BASE_DIR, "static_collected")

MEDIA_ROOT = os.path.join(BASE_DIR, 'static_collected/media')
MEDIA_URL = '/media/'

# import excel, csv, odt

FILE_UPLOAD_HANDLERS = ["django_excel.ExcelMemoryFileUploadHandler",
                        "django_excel.TemporaryExcelFileUploadHandler"]

AUTH_USER_MODEL = 'adm_security.Users'

LOGIN_REDIRECT_URL = '/'

ACCOUNT_ACTIVATION_DAYS = 7

DATA_UPLOAD_MAX_NUMBER_FIELDS = 1024000

X_FRAME_OPTIONS = 'ALLOWALL'