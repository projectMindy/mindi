/**
 * Created by PeruCore on 15/09/16.
 */
/*Preloader*/
$(window).load(function () {
    setTimeout(function () {
        /* === Preloader === */
        $("#preloader").fadeOut("slow");
    }, 300);
    cargar_url();
});

$(document).ready(function () {
    setTimeout(function () {
        M.toast({html: 'Bienvenido a mindi!'}, 2000)
    }, 2000);

    // CounterUp Plugin
    $('.counter').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 3500,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
                $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            }
        });
    });


});

$(window).hashchange(function () {
    cargar_url();
});

function cargar_url() {
    let str = window.location.hash;
    if (str.length >= 1) {
        str = str.substring(1, str.length);
        traemedatos(str);
        activarModulo(url);
        //$("#"+str).addClass('sfActive');
        //f = $("#"+str).attr('father');
        //$("#"+f).css('style','block');
    } else {
        traemedatos('dashboard/');
    }
}


function breadCrum(u) {
    let breadcrumb = u.split("/");
    let tam = breadcrumb.length;
    let html = '';
    let url = breadcrumb[0] + "/";
    for (let i = 1; i < tam; i++) {
        if (i === 3 || breadcrumb[i] === "") {
            break;
        }
        url += breadcrumb[i] + "/";
        html += '<li> <a href="#' + url + '"> ' + breadcrumb[i] + '</a> / </li>';
    }
    html = '<li><a href="#"><i class="fa fa-home"></i> Inicio</a> </li> /' + '<li><a> ' + breadcrumb[0] + '</a> </li>/' + html;
    $("#dashboard").html(html);
    return true;
}

function traemedatos(u) {
    breadCrum(u);
    if (addtab(u)) {
        let tab = u.split("/");
        let id = tab[0] + '-' + tab[1] + '-' + tab[2];
        $.ajax({
            type: 'GET',
            url: u,
            //data: {'sys':s},
            success: function (d) {
                let ht = "<div class='resultados' id='r-" + id + "'>" + d + "</div>";
                //$('#resultados').append(ht);
                $('#resultados').empty().html(ht);
            },
            error: function (xhr) {
                let ht = "<div class='resultados' id='r-" + id + "'>" + xhr.responseText + "</div>";
                //$('#resultados').append(ht);
                $('#resultados').empty().html(ht);
            },
            beforeSend: function () {
                // TODO: show spinner
                $("#preloader").fadeIn(400);
            },
            complete: function () {
                // TODO: hide spinner
                //$("#algo").attr('style', '');
                $('#preloader').fadeOut(400, "linear");

            }
        });
        //$(".resultados").css('display','NONE');
        //$("#r-"+id).css('display','block');
    }
}

function addtab(u) {
    let ur = [];
    let tabp = ".tab-p";
    $(tabp).each(function () {
        let id = $(this).attr('id');
        ur.push(id);
    });
    let tab = u.split("/");
    let tb;
    let pes;
    if (tab.length > 2) {
        tb = tab[2].split("?")[0];
        pes = tab[1] + '/' + tb;
    } else {
        tb = '';
        pes = tab[0];
    }
    let id = tab[0] + '-' + tab[1] + '-' + tb;
    if ($.inArray(id, ur) >= 0) {
        $(tabp).removeClass('tactive');
        $("#" + id).addClass('tactive');
        return true;
    }
    if ($(tabp).length > 3) {
        $("#tab-p").html("")
    }
    let html = '<div class="tab-p tactive"  id="' + id + '" >';
    html += '<a style="margin-right: 10px" ondblclick="location.hash = \'#' + u + '\';' +
        'fullscreen(document.getElementById(\'resultados\'))" href="#' + u + '" onclick="tabp(this,\'' + u + '\')">' +
        pes + '</a>';
    html += '<i class="material-icons close" onclick="closet(this)">close</i>';
    html += `<i class="material-icons tiny" href="#${u}" onclick="tabp(this,'${u}')">settings_backup_restore</i>`;
    html += '<i class="material-icons tiny" onclick="location.hash = \'#' + u + '\';' +
        'fullscreen(document.getElementById(\'resultados\'))">open_with</i>';
    html += '</div>';
    $(tabp).removeClass('tactive');
    $("#tab-p").append(html);
    return true;
}

function tabp(t, u) {
    $(".tab-p").removeClass('tactive');
    $(t).closest('div').addClass('tactive');
    traemedatos(u);
}

function closet(t) {
    $(t).closest('div').remove();
    if ($(t).hasClass('tactive')) {
        $("#resultados").empty();
    }

    let ur = [];
    $(".tab-p a").each(function () {
        let id = $(this).attr('href');
        ur.push(id);
    });

    if (ur.length > 0) {
        window.location.hash = ur[ur.length - 1];
    } else {
        window.location.hash = "#dashboard/";
    }
    //$("#r-"+$(t).closest('div').attr('id')).remove();
}

function noti(type, msg) {
    Lobibox.notify(type, {
        size: 'mini',
        rounded: true,
        delayIndicator: true,
        msg: msg, delay: 2500,
    });
}

function clickmenu(t, u) {

    $('a.active-page').each(function (a, i) {
        $(i).removeClass();
    });
    $(t).addClass('open');
    if (window.location.hash) {
        if (str == u) {
            return traemedatos(u);
        }
    }
    window.location.href = "#" + u;
}

function activarModulo(url) {
    let tab = url.split("/");
    if (tab[0] === "security") {
        $("#seguridad1").addClass('active');
        $("#seguridad2").addClass('active');
        $("#seguridad3").css('display', 'block');
        if (tab[1] === "profile") {
            $("#perfil_usuario").addClass('active-page');
        } else if (tab[1] === "modules") {
            $("#modulos").addClass('active-page');
        } else if (tab[1] === "usuarios") {
            $("#usuarios").addClass('active-page');
        } else if (tab[1] === "permission") {
            $("#permisos").addClass('active-page');
        }
    } else {
        $('#' + tab[0] + "1").addClass('active');
        $('#' + tab[0] + "2").addClass('active');
        $('#' + tab[0] + "3").css('display', 'block');
        $('#' + tab[1]).addClass('active-page');
    }

}

function atras() {
    window.history.back();
}

$(function () {
    $.fn.required = function () {
        if ($(this).val() === '' || $(this).val() === 0) {
            $(this).css('border', 'solid 2px red');
            $('#msg').html('<label class="lbl_msg">Debes llenar todos los campos necesarios</label>');
            $(this).focus();
            return false;
        } else {
            $(this).css('border', 'solid 1px #ccc');
            $('#msg').html('');
            return true;
        }
    };
});

let fullscreen = function (e) {
    if (e.webkitRequestFullScreen) {
        e.webkitRequestFullScreen();
    } else if (e.mozRequestFullScreen) {
        e.mozRequestFullScreen();
    }
};

function check_js(url) {
    let len = $('script').filter(function () {
        return ($(this).attr('src') === url);
    }).length;
    return len !== 0;

}

