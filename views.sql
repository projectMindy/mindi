drop view if exists bcp_udv_int.HD_MOVIMIENTOBCAMOVIL;
create view bcp_udv_int.HD_MOVIMIENTOBCAMOVIL as
select
    codclavepartycli as codclavecic,
    null as codempcredipago,
    null as desdetalleservicio,
    fecdia as fecdia,
    fecinteraccion as fectransaccion,
    flgvalida as flgtransaccionvalida,
    horinteraccion as hortransaccion,
    numinteraccionapp as numtransaccionbcamovil,
    tipinteraccion as tiptransaccionbcamovil

from bcp_udv_int.H_INTERACCIONBANCAMOVIL

---

drop view if exists bcp_udv_int.MM_DESTIPODOCUMENTO;
create view bcp_udv_int.MM_DESTIPODOCUMENTO as
select
    tippartyidentificacion as tipdoc,
    destippartyidentificacion as destipdoc

from bcp_udv_int.M_DESTIPOPARTYIDENTIFICACION

---

drop view if exists bcp_udv_int.HD_MOVHOMEBANKINGTRANSACCION;
create view bcp_udv_int.HD_MOVHOMEBANKINGTRANSACCION as
select
    fecinteraccion as fecdia,
    codclavectaorigen as codclaveopectaorigen,
    numdocserv as numdocservicio,
    codclavectadestino as codclaveopectadestino,
    codretornoopeapp as tipresultadotransaccion,
    numinteraccionapp as codinternotransaccionhb

from bcp_udv_int.H_INTERACCIONBANCAINTERNET


---

drop view if exists bcp_udv_int.HD_MOVIMIENTOAGENTEVIABCP;
create view bcp_udv_int.HD_MOVIMIENTOAGENTEVIABCP as
select
    b.codagente as codagenteviabcp,
    b.codclavectadestino as codclaveopectaabono,
    b.codclavectaorigen as codclaveopectacargo,
    b.codctacomercialdestino as codopectaabono,
    b.codctacomercialorigen as codopectacargo,
    null as codserviciopagado,
    b.codtarjeta,
    b.fecinteraccion as fecdia,
    b.horinteraccion as hortransaccion,
    b.nbremprecaudadora as nbrempresarecaudadora,
    b.codclavesecuencial as numregistro,
    b.tipestadointeraccion as tipesttransaccionagenteviabcp,
    b.tipinteraccion as tiptransaccionagenteviabcp
from bcp_udv_int.H_INTERACCIONAGENTEDAC as a inner
    bcp_udv_int.H_INTERACCIONAGENTE as b on
    a. = b.

---

drop view if exists bcp_udv_int.MD_AFILIACIONCREDIPAGO;
create view bcp_udv_int.MD_AFILIACIONCREDIPAGO as
select
    codemprecaudadora as codempcredipago,
    codcontrato as codafiliacioncredipago,
    codclavecta as codclaveopecta,
    flgregeliminadofuente as flgregeliminado

from bcp_udv_int.H_INTERACCIONBANCAINTERNET

---

drop view if exists bcp_udv_int.MD_AFILIACIONCREDIPAGOXCANAL;
create view bcp_udv_int.MD_AFILIACIONCREDIPAGOXCANAL as
select
    codrubrorecaudacion as codrubrocredipago,
    codclavecontrato as codafiliacioncredipago,
    descomercialrecaudacion as nbrcomercialemprecaudadora,
    flgregeliminadofuente as flgregeliminado,
    codcanal

from bcp_udv_int.M_DETALLERECAUDACION


---

drop view if exists bcp_udv_int.MD_CUENTA;
create view bcp_udv_int.MD_CUENTA as
select
    codclavecta as codclaveopecta,
    codclavepartycli as codclavecic,
    flgregeliminadofuente as flgregeliminado,
    fecdia

from bcp_udv_int.M_CUENTAFINANCIERA

---

drop view if exists bcp_udv_int.MD_DESCODRUBROCREDIPAGO;
create view bcp_udv_int.MD_DESCODRUBROCREDIPAGO as
select
    codrubrorecaudacion as codrubrocredipago,
    desrubrorecaudacion as descodrubrocredipago,
    flgregeliminadofuente as flgregeliminado

from bcp_udv_int.M_DESRUBRORECAUDACION

--

drop view if exists bcp_udv_int.MD_CLIENTEMARCALPDP;
create view bcp_udv_int.MD_CLIENTEMARCALPDP as
select
    codclavepartycli as codclavecic,
    tipconsentimientoleyprotecciondatogrupocredicorp as tipconsentimientogrupo

from bcp_udv_int.M_CLIENTE


