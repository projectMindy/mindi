from django.apps import AppConfig


class AdmSecurityConfig(AppConfig):
    name = 'apps.adm_security'
