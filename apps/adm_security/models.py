from django.db import models
from apps.security.models import Users_Core
from apps.adm_institucion.models import Institucion

class Users(Users_Core):
    institucion     = models.ForeignKey(Institucion,blank=True,null=True,on_delete=models.CASCADE,)

    def __validate_users_in_institucion__(**kwargs):
        return Users.objects.filter(**kwargs)

    def list_all_users(l):
            l['status'] = True
            d = Users.objects.filter(**l)
            return d
