from django.contrib import admin
# Register your models here.
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import get_user_model

Users = get_user_model()

from apps.security.forms import UserChangeForm, UserCreationForm


class ProfileInlineAdmin(admin.TabularInline):
    model = Users.profiles.through


"""class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username', 'email', 'dni', 'is_admin', 'birth_date')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('username', 'email')}),
        ('Foto Perfil', {'fields': ('foto',)}),
        ('Información Personal', {'fields': ('names', 'first_surname', 'second_surname', 'dni','institucion')}),
        # ('Redes Sociales', {'fields': ('redsocial',)}),
        ('Permisos', {'fields': ('is_admin', 'is_active')}),
    )
    inlines = (ProfiileInlineAdmin,)
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'dni', 'password1', 'password2')}
         ),
    )
    search_fields = ('email', 'dni', 'username')
    ordering = ('email', 'dni', 'username')
    filter_horizontal = ()
"""


# class permisosInlineAdmin(admin.TabularInline):
# model = perfil.modulo.through

# class perfilAdmin(admin.ModelAdmin):
# inlines = (permisosInlineAdmin,)

class CustomUserAdmin(UserAdmin):
    form = UserChangeForm
    add_form = UserCreationForm
    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username', 'email', 'dni', 'is_admin', 'birth_date')
    list_filter = ('is_admin',)
    fieldsets = (
        (None, {'fields': ('username', 'email')}),
        ('Foto Perfil', {'fields': ('foto',)}),
        ('Información Personal', {'fields': (
            'names', 'first_surname', 'second_surname', 'dni', 'birth_date', 'sex', 'institucion'
        )}),
        # ('Redes Sociales', {'fields': ('redsocial',)}),
        ('Permisos', {'fields': ('is_admin', 'is_active')}),
    )
    inlines = (ProfileInlineAdmin,)
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'dni', 'names', 'first_surname', 'second_surname', 'birth_date', 'sex',
                'username', 'email',  'password1', 'password2', 'institucion'
            )}
         ),
    )
    search_fields = ('email', 'dni', 'username')
    ordering = ('email', 'dni', 'username')
    filter_horizontal = ()


# Now register the new UserAdmin...
admin.site.register(Users, CustomUserAdmin)
