# Generated by Django 2.0.5 on 2018-05-15 04:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('adm_security', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='users',
            name='foto',
            field=models.ImageField(blank=True, max_length=500, null=True, upload_to='fotoperfil'),
        ),
    ]
