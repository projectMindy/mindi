# Generated by Django 2.0.5 on 2018-05-15 02:04

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0009_alter_user_last_name_max_length'),
        ('adm_institucion', '0001_initial'),
        ('security', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Users',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('status', models.BooleanField(default=True)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted_at', models.DateTimeField(blank=True, null=True)),
                ('group_blood', models.CharField(blank=True, max_length=5, null=True)),
                ('ruc', models.CharField(blank=True, max_length=13, null=True)),
                ('civil_status', models.CharField(choices=[('soltero', 'soltero'), ('casado', 'casado'), ('divorciado', 'divorciado'), ('viudo', 'viudo')], default='Soltero', max_length=15)),
                ('username', models.CharField(error_messages={'unique': 'Ya existe usuario con ese nombre'}, help_text='Requiere: 30 carácteres o menos. Letras, digitos y  @/./+/-/_ sólo.', max_length=30, unique=True, validators=[django.core.validators.RegexValidator('^[\\w.@+-]+$', 'Ingrese un usuario válido. Este valor solo podrá contener Letras, Números and @/./+/-/_ Carácteres.')], verbose_name='Usuario')),
                ('names', models.CharField(max_length=50, null=True, verbose_name='Nombres')),
                ('first_surname', models.CharField(max_length=50, null=True, verbose_name='Apellido Paterno')),
                ('second_surname', models.CharField(max_length=50, null=True, verbose_name='Apellido Materno')),
                ('dni', models.CharField(error_messages={'unique': 'Ya existe dni, probar con otro'}, help_text='DNI Requiere: 8 carácteres obligatorios', max_length=8, unique=True, validators=[django.core.validators.RegexValidator('^[\\w.@+-]+$', 'Ingrese un dni válido. Este valor solo podrá contener Númerosde 8 Dígitos.')])),
                ('email', models.EmailField(blank=True, max_length=255, null=True, verbose_name='Dirección de email')),
                ('birth_date', models.DateField(null=True, verbose_name='Fecha de Nacimiento')),
                ('foto', models.ImageField(max_length=500, null=True, upload_to='fotoperfil')),
                ('cellphone', models.CharField(max_length=100, null=True, verbose_name='Celular')),
                ('telephone', models.CharField(max_length=100, null=True)),
                ('address', models.CharField(max_length=200, null=True)),
                ('work_experiencie', models.CharField(max_length=30, null=True)),
                ('sex', models.CharField(choices=[('f', 'Femenino'), ('m', 'Masculino')], max_length=1)),
                ('password_default', models.CharField(max_length=15, null=True)),
                ('is_active', models.BooleanField(default=True, help_text='Designa si el usuario debe ser tratado como activo. Seleccionarla en lugar de eliminar las cuentas .', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='fecha de Inscripción')),
                ('is_admin', models.BooleanField(default=False)),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('institucion', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='adm_institucion.Institucion')),
                ('permissions', models.ManyToManyField(blank=True, to='security.permission')),
                ('profiles', models.ManyToManyField(blank=True, to='security.profile')),
                ('ubigeo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='security.ubigeo')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
