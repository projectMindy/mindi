# coding=utf-8
import json

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import HttpResponse
from django.urls import reverse_lazy

from apps.adm_test.forms import f_Test_Programed
from apps.adm_test.models import History_test_Patient, Test_Programed

from apps.security.core.crud import ListBase, ResponseLoginView
from .models import (Questions, Answers, TestPsicologico, Results, DetalleAltPreg, Valoracion_sistema, Alternativas,
                     DetalleVarTest, Dimensions, Diagnostics, RangosPercentiles, Informaci_alumno, situacion_familiar,
                     respuestas_fpe, ImagesQuestion, DetalleImagPreg)
from apps.security.core.processing_test import preProcesamiento
import random
from django.shortcuts import render


@login_required(login_url='/login/')
def clear_answer(request):
    Answers.objects.filter(history=request.GET.get('patient')).delete()
    return HttpResponse('DATOS ELIMINADOS')


def testTemplate(r):
    order = 'correlativo'

    pre = Questions.objects.filter(status=True, idtest_id=r.GET.get('test')).order_by(
        order)  # query for get values of Questions
    dalt = DetalleAltPreg.objects  # query for get images from of detail
    li = []  # new list for create json
    for nl in pre.values():
        da = dalt.filter(preguntas_id=nl['id']).values('id', 'puntaje',
                                                       'alternativas__descripcion',
                                                       'alternativas__acronimo', 'alternativas__escala',
                                                       'alternativas__tipo__nombre',
                                                       'alternativas__puntajeAsignado', 'alternativas__imagen',
                                                       'alternativas__iddimension_id',
                                                       'alternativas__iddimension__nombre'
                                                       ).order_by('puntaje')
        nl['alternativas'] = list(da)
        li.append(nl)  # saved all questions with your alternatives
    return render(r, "Test/newtemplate.html", {'questions': li, 'cantQ': len(li),
                                               'history': r.GET.get('pat'),
                                               'test': TestPsicologico.objects.filter(id=r.GET.get('test')).values()}
                  )

    # mis test


class MisTest(ListBase):
    template_name = 'Test/MisTest.html'

    def l_queryset(self):
        return History_test_Patient.__test__programed__byusers__(self)


class ShowTest(MisTest):
    template_name = 'Test/ShowTest.html'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        h = History_test_Patient.objects.filter(id=self.request.GET.get('pat')).values()
        if (h[0]["status_test"]) == "Por Diagnosticar":
            return HttpResponseRedirect(reverse_lazy('listartest'))
        return self.render_to_response(context)


class QuestionsTest(ResponseLoginView):
    template_name = 'Test/ShowTest.html'

    def l_queryset(self):
        return Questions.__get__questions__list__(self)

    def response(self, response):
        return self.response_json()


def json_answer(r):
    consulta = TestPsicologico.prepararTest(r, r.GET.get('test'))
    return HttpResponse(json.dumps(list(consulta)))


class ListDiagnostic(ListBase):
    template_name = "Test/list_diagnostics.html"

    def l_queryset(self):
        return History_test_Patient.objects.filter(
            test_programed__id=self.request.GET.get('test_programed'),
            status_test="Por Diagnosticar"
        )


class GenerateDiagnostic(ResponseLoginView):
    def response(self, context):
        idtest = History_test_Patient.objects.values('test_programed__test'). \
            filter(id=self.request.POST.get('history'))[0]['test_programed__test']
        preProcesamiento(self.request, idtest)
        return HttpResponse("Bien")


class GenerateDiagnosticAll(ResponseLoginView):
    def response(self, context):
        ListHistory = History_test_Patient.objects.values('id'). \
            filter(test_programed__id=self.request.POST.get('test_pro'), status_test="Por Diagnosticar")

        idtest = Test_Programed.objects.values('test') \
            .filter(id=self.request.POST.get('test_pro'))[0]['test']

        self.request.POST._mutable: bool = True
        for h in ListHistory:
            self.request.POST['history'] = h['id']
            preProcesamiento(self.request, idtest)
        return HttpResponse("Bien")


class save_test(ResponseLoginView):
    def response(self, response):
        print(self.request.POST)
        if "answer[]" in self.request.POST.keys():
            for a in self.request.POST.getlist("answer[]"):
                dixy = {
                    'answer': a,
                    'value_marked': self.request.POST.get('value_marked'),
                    'history': self.request.POST.get('history'),
                    'time_marked': self.request.POST.get('time_marked'),
                }
                Answers.saveMultipleAnswer(self, dixy)
                h = History_test_Patient.objects.get(pk=self.request.POST.get('history'))
                h.status_test = "Por Diagnosticar"
                h.save()
            print("guardando")
        else:
            try:
                if int(self.request.POST.get('tiempo_ejecucion')) > int(self.request.POST.get('time_marked')):
                    print("milton")
                    return HttpResponse("Falta completar el tiempo de espera par marcar")
            except ValueError as e:
                pass
            Answers.saveAnswer(self)
            if self.request.POST.get('termino') == 'ya termine':
                h = History_test_Patient.objects.get(pk=self.request.POST.get('history'))
                h.status_test = "Por Diagnosticar"
                h.save()
                '''Idtest = History_test_Patient.objects.values('test_programed__test'). \
                    filter(id=self.request.POST.get('history'))[0]['test_programed__test']
                preProcesamiento(self.request, Idtest)
                # Results.enviarAProcesamiento(Results(), self.request, Idtest)'''
        return HttpResponse("Bien")


def datos_aleatoriosAF5ki(r):
    # determinar el id historial, para eso se ingresara el id de la programacion
    # este dato es estatico XD, se tiene que modificar
    idprogramacion = 35;
    # consultamos a todos los estudiantes que estan en esa progamacion
    historial = History_test_Patient.objects.values('id').filter(test_programed=idprogramacion)
    return HttpResponse(json.dumps(list(historial)))


def llenar_base(r):
    k = 7
    l = 318
    for j in range(163, 223):
        for n in range(1, 3):
            l = l + 1
            k = k + 1
            holis = DetalleAltPreg()
            holis.id = l
            holis.status = True
            holis.alternativas_id = k
            holis.preguntas_id = j
            holis.save()
    return HttpResponse("listo")


def emocional_answer(r):
    consulta = TestPsicologico.prepararTest('prueba')

    return HttpResponse(json.dumps(list(consulta)))


def guardar_af5(request):
    # Debes validar la primera entrada
    if Answers.validarRespuestas(Answers, request) == True:
        Answers.saveAnswer(Answers, request)
        if request.POST.get('bandera') == 'yes':
            Results.enviarAProcesamiento(Results(), request)
        return HttpResponse("Bien")
    else:
        return HttpResponse("Mal")


def procesarAf5(request):
    # print("llego")
    # print(request.POST)
    return HttpResponse("holi")


class result(ListBase):
    template_name = "result.html"
    def l_queryset(self):
        '''h = History_test_Patient.objects.get(pk=self.request.GET.get('hist'))
        h.status_test = "Realizado"
        h.save()'''
        return Results.objects.filter(history=self.request.GET.get('hist'), status=True).order_by('-score')


class done_test(result):
    template_name = "done.html"


def valor_sistema(request):
    return render(request, 'valoracion_sistema.html', {'historial': request.GET.get('historial')})


def guardar_valor(request):
    re = Valoracion_sistema()
    re.idusur = request.user.id
    re.idtest = 1
    re.idhistorial = request.POST.get('historial')
    re.question1 = request.POST.get('p1')
    re.question2 = request.POST.get('p2')
    re.question3 = request.POST.get('p3')
    re.question4 = request.POST.get('p4')
    re.question5 = request.POST.get('p5')
    re.save()
    return render(request, "mensajevaloracion.html")


def migrar_valora(request):
    datos = History_test_Patient.objects.filter(test_programed=32, patient__profiles=6).exclude(
        answers__value_marked__isnull=True).values("id", "patient")
    cont = 0
    with open('E:\datoscachay.csv', 'r') as f:
        file = f.read().splitlines()
        for l in file:
            file = l.split(";")
            re = Valoracion_sistema()
            re.idusur = datos[cont]["patient"]
            re.idtest = 1
            re.idhistorial = datos[cont]["id"]
            re.question1 = file[0]
            re.question2 = file[1]
            re.question3 = file[2]
            re.question4 = file[3]
            re.question5 = file[4]
            re.save()
            cont = cont + 1
    pass


def consumir_api(r):
    f = open('C:/Users/User/Documents/test json/cia.json', 'r')
    mensaje = f.read()
    datos = json.loads(mensaje)
    # guardar testpsicologico
    test = TestPsicologico()
    test.name = datos["test"]["name"]
    test.description = datos["test"]["description"]
    test.instructions = datos["test"]["instructions"]
    test.level_correction = datos["test"]["level_correction"]
    test.save()

    array_alternativa = guardar_alternativa(datos["preguntas"][0]["alternativas"])

    # guardar preguntas
    for foo in datos["preguntas"]:

        preg = Questions()
        preg.descripcion = foo["descripcion"]
        preg.correlativo = foo["correlativo"]
        preg.tiempo_ejecucion = foo["tiempo_ejecucion"]
        preg.idtest_id = test.id
        preg.save()
        for woo in foo["imagenes_pregunta"]:
            # guardar imagen
            ima = ImagesQuestion()
            ima.description = "imagen"
            ima.image = woo
            ima.save()

            # guardar detalle
            dip = DetalleImagPreg()
            dip.imagenes_id = ima.id
            dip.preguntas_id = preg.id
            dip.save()

        for doo in array_alternativa:
            # guardar alternativa
            # guardar detalle alternativa
            dalt = DetalleAltPreg()
            dalt.alternativas_id = doo["id"]
            dalt.preguntas_id = preg.id
            dalt.puntaje = int(eval(doo["puntaje"]))
            dalt.save()

    # gudar detallevariable
    detvar = DetalleVarTest()
    detvar.tests_id = test.id
    detvar.variables_id = 1
    detvar.save()

    # guardado de dimension
    for too in datos["test"]["dimensiones"]:
        dim = Dimensions()
        dim.nombre = too["nombre"]
        dim.descripcion = too["descripcion"]
        dim.formula = too["formula"]
        dim.test_id = test.id
        dim.save()
        # guadado de diagnostico
        for yoo in too["diagnosticos"]:
            dia = Diagnostics()
            dia.nombre = yoo["nombre"]
            dia.descripcion = yoo["descripcion"]
            dia.color = yoo["color"]
            dia.dimensiones_id = dim.id
            dia.save()

            # guardado de centiles
            for woo in yoo["centiles"]:
                per = RangosPercentiles()
                per.dimensiones_id = dim.id
                per.diagnostics_id = dia.id
                per.det_variables_id = detvar.id
                per.puntuacion = woo["puntuacion"]
                per.save()
    pass


def guardar_alternativa(too):
    id = {}
    arr = []
    for doo in too:
        alt = Alternativas()
        alt.descripcion = doo["alternativas__descripcion"]
        alt.acronimo = doo["alternativas__acronimo"]
        alt.tipo_id = 1
        alt.imagen = doo["alternativas__imagen"]
        alt.puntajeAsignado = doo["alternativas__puntajeAsignado"]
        alt.save()

        doc = dict(id=alt.id, puntaje=doo["alternativas__puntajeAsignado"])
        arr.append(doc)
    return arr


def ficha_estudiante(r):
    test = r.GET.get("test")
    pat = r.GET.get("pat")
    return render(r, "ficha_estudiante.html", {"test": test, "pat": pat})


def guardar_ficha_estudiante(r):
    # guardar informacion alumno
    ifam = Informaci_alumno()
    ifam.lugar_nacimiento = r.POST.get("lugar_nacimiento")
    ifam.direcciion_actual = r.POST.get("direcciion_actual")
    ifam.history_id = r.POST.get("pat")
    ifam.save()

    # guardar situación familiar
    # falta crear al for para recorrer
    con = 0
    for datos in r.POST.getlist("Nombre_familia[]"):
        sfam = situacion_familiar()
        sfam.Nombre_familia = datos
        sfam.parentesco = r.POST.getlist("parentesco[]")[con]
        sfam.ocupacion = r.POST.getlist("ocupacion[]")[con]
        sfam.Edad = r.POST.getlist("Edad[]")[con]
        sfam.history_id = r.POST.get("pat")
        sfam.save()
        con = con + 1

    # guardar preguntas del 2 al 44
    for w in range(2, 45):
        d = str(w)
        pre2 = respuestas_fpe()
        pre2.history_id = r.POST.get("pat")
        pre2.correlativo_pregunta = w
        pre2.respuesta = r.POST.get(d)
        pre2.save()

    h = History_test_Patient.objects.get(pk=r.POST.get('pat'))
    h.status_test = "Por diagnosticar"
    h.save()

    return HttpResponse("Bien")


def guardar_imagen(r):
    # test Ave, Clima laboral[2,3]
    preguntaAve = Questions.objects.values("id", "correlativo").filter(idtest=3).order_by("correlativo")
    print(preguntaAve)
    for foo in preguntaAve:
        print(foo["id"])
        ipre = ImagesQuestion()
        ipre.description = "Clima laboral" + str(foo["correlativo"])
        ipre.image = "test/TEST CL/" + str(foo["correlativo"]) + "A.jpg"
        ipre.save()

        detalleimagein(ipre.id, foo["id"])

        ipre2 = ImagesQuestion()
        ipre2.description = "Clima laboral" + str(foo["correlativo"])
        ipre2.image = "test/TEST CL/" + str(foo["correlativo"]) + "B.jpg"
        ipre2.save()

        detalleimagein(ipre2.id, foo["id"])


def detalleimagein(e, s):
    deta = DetalleImagPreg()
    deta.imagenes_id = e
    deta.preguntas_id = s
    deta.save()


# insertar imagenes identificando su correlativo
def guardar_imagenes_correlativo(r):
    f = open('C:/Users/User/Documents/test json/cia.json', 'r')
    mensaje = f.read()
    datos = json.loads(mensaje)
    preguntas = Questions.objects.values("id", "correlativo").filter(idtest=5).order_by("correlativo")
    for foo in preguntas:
        for woo in datos["preguntas"]:
            if (int(woo["correlativo"]) == int(foo["correlativo"])):
                for doo in woo["imagenes_pregunta"]:
                    # guardamos la imagenes por preguntas
                    ima = ImagesQuestion()
                    ima.description = "imagen"
                    ima.image = doo
                    ima.save()

                    # guardar detalle imagense preguntas
                    dip = DetalleImagPreg()
                    dip.imagenes_id = ima.id
                    dip.preguntas_id = foo["id"]
                    dip.save()
