from django.contrib import admin
from apps.security.core.admin import BaseAdmin
from .models import Questions, RangeAge, ImagesQuestion, Dimensions, TestPsicologico, \
    Alternativas, Diagnostics, TipoAlternativas, RangosPercentiles, Variables, CategoriaVariables, Parameters


class SubDimensionesFilter(admin.SimpleListFilter):
    title = 'dimensiones'
    parameter_name = 'dimensiones__id__exact'

    def lookups(self, request, model_admin):
        dimensiones = Dimensions.objects
        if 'idtest__id__exact' in request.GET:
            dimension = dimensiones.filter(test=request.GET['idtest__id__exact'])
        else:
            dimension = dimensiones.all()
        return (
            (subd.pk, subd.nombre) for subd in dimension
        )

    def queryset(self, request, queryset):
        if 'dimensiones__id__exact' in request.GET:
            return queryset.filter(dimensiones=request.GET['dimensiones__id__exact'])


class PreguntaInlineAdmin(admin.TabularInline):
    model = Questions.imagen.through


class AlternativaInlineAdmin(admin.TabularInline):
    model = Questions.alternativa.through

    def get_formset(self, request, obj=None, **kwargs):
        self.exclude = ['deleted_at', 'status']
        return super(AlternativaInlineAdmin, self).get_formset(request, obj, **kwargs)


class PreguntaAdmin(BaseAdmin):
    inlines = (PreguntaInlineAdmin, AlternativaInlineAdmin,)
    list_display = ('id', 'descripcion', 'correlativo')
    list_filter = (
        'idtest', SubDimensionesFilter
    )


class TestInlineAdmin(admin.TabularInline):
    model = TestPsicologico.variable.through
    verbose_name = "Variables"

    def get_formset(self, request, obj=None, **kwargs):
        self.exclude = ['deleted_at', 'status']
        return super(TestInlineAdmin, self).get_formset(request, obj, **kwargs)


class TestAdmin(BaseAdmin):
    inlines = (TestInlineAdmin,)


class RangoAdmin(BaseAdmin):
    list_filter = (
        'det_variables', 'dimensiones'
    )


class DimensionesAdmin(BaseAdmin):
    list_filter = (
        'test',
    )


class DiagnosticoAdmin(BaseAdmin):
    list_filter = (
        'dimensiones__test',
        'dimensiones'
    )


# Register your models here.
admin.site.register(Questions, PreguntaAdmin)
admin.site.register(TestPsicologico, TestAdmin)
admin.site.register(RangeAge)
admin.site.register(ImagesQuestion)
admin.site.register(Dimensions, DimensionesAdmin)
admin.site.register(Alternativas, BaseAdmin)
admin.site.register(Diagnostics, DiagnosticoAdmin)
admin.site.register(TipoAlternativas, BaseAdmin)
admin.site.register(Variables, BaseAdmin)
admin.site.register(CategoriaVariables, BaseAdmin)
admin.site.register(RangosPercentiles, RangoAdmin)
admin.site.register(Parameters)
