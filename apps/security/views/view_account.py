import json
import random
import string

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import get_user_model, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import (
    PasswordChangeForm
)
from django.urls import reverse
from django.urls import reverse_lazy
from django.http import HttpResponseBadRequest, HttpResponseRedirect
from django.shortcuts import render, HttpResponse, get_object_or_404
from django.shortcuts import resolve_url
from django.template.response import TemplateResponse
from django.views.generic import TemplateView, UpdateView
from django_registration.backends.activation.views import RegistrationView

# from rest_framework.decorators import api_view, renderer_classes, permission_classes
# from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer

Users = get_user_model()
from apps.security.core import error
from django.core.mail import send_mail
# from rest_framework import permissions
from apps.security.models import profile, permission, modules
from apps.security.forms import cueform, formFoto, UsuarioCreationForm
from apps.security.core.pagination.num_pag import paginacion
from django.db.models import Count
from django.views.generic import ListView


class all_users(LoginRequiredMixin, ListView):
    login_url = reverse_lazy('login')

    template_name = "system/users/l_users.html"
    model = Users
    queryset = Users.objects.filter(status=True)

    def get_context_data(self, **kwargs):
        c = super(all_users, self).get_context_data(**kwargs)
        prof = profile.objects.filter(status=True)
        l = {}
        if self.request.GET.get('profiles__id'):
            l["profiles__id"] = self.request.GET.get('profiles__id')

        queryset = Users.list_all_users(l)

        c.update({'profiles': prof, 'object_list': queryset})

        return c


@login_required(login_url="login/")
def updateCuenta(q):
    if q.method == "POST":
        a = get_object_or_404(Users, pk=q.user.id)
        # Users_Core.objects.get(pk=q.user.id) get_object_or_404 es igual a esto
        form = cueform(q.POST, instance=a)
        if form.is_valid():
            new_choice = form.save(commit=False)
            new_choice.institucion_id = q.POST.get('institucion_id')
            new_choice.save()
            return HttpResponse("guardado")
        else:
            return error(form)
    else:
        a = q.user.id
        fa = get_object_or_404(Users, pk=int(a))
        f = cueform(instance=fa)
        return render(q, "system/formcuenta.html", {"form": f})


class editarCuenta(LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy('error404')
    template_name = 'system/account/cuenta.html'


class dashboard(LoginRequiredMixin, TemplateView):
    template_name = "system/dashboard.html"


@login_required(login_url='login/')
def password_change(request,
                    template_name='system/account/contra_usu.html',
                    post_change_redirect=None,
                    password_change_form=PasswordChangeForm,
                    extra_context=None):
    if post_change_redirect is None:
        post_change_redirect = reverse('password_change_done')
    else:
        post_change_redirect = resolve_url(post_change_redirect)
    if request.method == "POST":
        form = password_change_form(user=request.user, data=request.POST)
        # verificar(request.POST['new_password2'],request.user.id)

        if form.is_valid():
            f = form.save()
            u = Users.objects.get(pk=f.id)
            u.password_default = request.POST['new_password2']
            u.save()
            # Updating the password logs out all other sessions for the user
            # except the current one if
            # django.contrib.auth.middleware.SessionAuthenticationMiddleware
            # is enabled.
            update_session_auth_hash(request, form.user)
            return HttpResponseRedirect(post_change_redirect)
        else:
            print("this see you, because there bug")
            ee = {}
            for i in form.errors:
                e = form.errors[i]
                ee[i] = str(e)
            return HttpResponseBadRequest(json.dumps(ee))
    else:
        form = password_change_form(user=request.user)
    context = {
        'form': form,
        'title': ('Cambio de Contraseña'),
    }
    if extra_context is not None:
        context.update(extra_context)

    return TemplateResponse(request, template_name, context)


@login_required(login_url='login/')
# @api_view(['GET','POST'])
# @permission_classes((permissions.AllowAny,))
# @renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def cuentaedi(q):
    a = q.user.id
    fa = get_object_or_404(Users, pk=int(a))
    f = cueform(instance=fa)
    return render(q, "system/formcuenta.html", {"f": f})


def datoscuenta(q):
    return render(q, "system/account/datoscuenta.html")


# contraseña aleatoria
def id_generator(size=8, chars=string.ascii_uppercase + string.digits + string.punctuation):
    return ''.join(random.choice(chars) for _ in range(size))


@login_required(login_url='login/')
def list_users(r):
    if r.method == 'POST':
        sys = r.POST.get("sys")
    else:
        sys = r.GET.get("sys")

    template_name = "system/users/t_users.html"
    u = Users.objects.filter(status=True)
    l = {'search': "'Users_Core','[\"names\",\"second_surname\",\"dni\",\"first_surname\"]'"}
    v = {'values': "'None'", 'template': "'datoAjaxusers.html'", }

    l.update(v)

    return paginacion(r, u, "", template_name, l)


@login_required(login_url='login/')
def list_users_profile(r):
    if r.method == 'POST':
        sys = r.POST.get("sys")
    else:
        sys = r.GET.get("sys")

    template_name = "system/users/t_users_profile.html"
    np = profile.objects.filter(status=True, id=r.GET.get('id')).values('description', 'id')[:1][0]
    u = Users.objects.filter(status=True, profiles=r.GET.get('id')).exclude(id=1).exclude(id=44).exclude(id=45)
    # print(u.query)
    l = {'search': "'Users_Core','[\"names\",\"second_surname\",\"dni\",\"first_name\"]'"}
    v = {'values': "'None'", 'template': "'datoAjaxusersProfile.html'", 'profile': np['description'], 'idp': np['id']}
    l.update(v)
    return paginacion(r, u, "", template_name, l)


class about_users(LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy('error404')
    template_name = "system/account/about_user.html"

    def get_context_data(self, **kwargs):
        context = super(about_users, self).get_context_data(**kwargs)
        u = Users.objects.filter(status=True, id=self.request.GET.get("id"))[:1]
        context['Users_Core'] = u

        return context


@login_required(login_url='login/')
def deleteuser(em, id):
    e = Users.objects.get(pk=id)
    e.status = False
    e.save()
    return HttpResponse("eliminado correctamente")


@login_required(login_url='login/')
def list_usersfalso(r):
    template_name = "system/users/t_users_profile.html"
    u = Users.objects.filter(status=False)
    l = {'search': "'users','[\"names\",\"second_surname\",\"dni\",\"first_surname\"]'"}
    v = {'values': "'None'", 'template': "'datoAjaxUsers_CoreProfile.html'", }
    l.update(v)
    return paginacion(r, u, "", template_name, l)


@login_required(login_url='login/')
def deleteuserall(em):
    for i in em.GET.getlist("allDel[]"):
        e = Users.objects.get(pk=i)
        e.status = False
        e.save()

    return HttpResponse("eliminado correctamente")


def recuperarall(em):
    print(em.GET.getlist("allDel[]"))
    for i in em.GET.getlist("allDel[]"):
        e = Users.objects.get(pk=i)
        e.status = True
        e.save()

    return HttpResponse("eliminado correctamente")


class add_User(LoginRequiredMixin, RegistrationView):
    login_url = reverse_lazy('error404')
    form_class = UsuarioCreationForm
    template_name = "system/users/form_user.html"

    def get_context_data(self, **kwargs):
        context = super(add_User, self).get_context_data(**kwargs)
        context['name'] = 'Registrar'
        return context

    def get_success_url(self, user):
        return ('/security/usuarios/completado', (), {})

    def form_invalid(self, form):
        ee = {}
        if form.errors:
            for i in form.errors:
                e = form.errors[i]
                ee[i] = str(e)
        return HttpResponseBadRequest(json.dumps(ee))

    def create_inactive_user(self, form):
        p = form.cleaned_data["dni"]
        new_user = form.save(commit=False)
        new_user.is_active = True
        new_user.username = form.cleaned_data["dni"]
        new_user.email = form.cleaned_data["email"]
        new_user.cellphone = form.cleaned_data["cellphone"]
        new_user.telephone = form.cleaned_data["telephone"]
        new_user.password_default = p
        new_user.set_password(p)
        new_user.save()
        for i in self.request.POST.getlist('profiles'):
            new_user.profiles.add(int(i))

        if self.request.POST.get('email'):
            send_mail(
                'Mindi le da la bienvenida!, ¡Gracias por su preferencia!',
                'la página web del sistema es: 0775.mindi.com <br> su usuario es: .' + str(
                    form.cleaned_data["dni"]) + ' y su contraseña es: ' + str(p),
                'mindisoft@gmail.com',
                [form.cleaned_data["email"]],
                fail_silently=False,
            )
        return new_user


class register_users_type(add_User):
    template_name = 'system/users/form_users_type.html'

    def get_context_data(self, **kwargs):
        context = super(register_users_type, self).get_context_data(**kwargs)
        context['name'] = 'Registrar'
        context['profile'] = self.request.GET.get('nombrep')
        context['idp'] = self.request.GET.get('idp')
        return context


def verificar(a, b):
    print(b)
    final = ''
    for i in a:
        dato = ord(i)
        dat = seleccionar()
        final = final + '' + str(dato) + '' + str(dat)
    if pago_rec.objects.filter(cod=b).exists():
        q = pago_rec.objects.get(cod=b)
        q.descripcion = final
        q.save()
    else:
        g = pago_rec()
        g.cod = b
        g.descripcion = final
        g.save()
    return "guardado"


class update_users(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('error404')
    template_name = "system/users/form_user.html"
    model = Users
    form_class = UsuarioCreationForm
    exclude = ('password1', 'password1')
    success_url = reverse_lazy('user-completado')

    def get_context_data(self, **kwargs):
        context = super(update_users, self).get_context_data(**kwargs)
        context['name'] = 'Actualizar'
        return context

    def form_invalid(self, form):
        ee = {}
        if form.errors:
            for i in form.errors:
                e = form.errors[i]
                ee[i] = str(e)
        return HttpResponseBadRequest(json.dumps(ee))


@login_required(login_url='login/')
def addprofile(r):
    new_user = Users.objects.get(pk=int(r.POST['usuario']))
    new_user.profiles.add(int(r.POST['perfil']))
    return HttpResponse("agregado")


@login_required(login_url='login/')
def fotoperfil(r):
    a = get_object_or_404(Users, pk=r.user.id)
    formu = formFoto(r.POST, r.FILES, instance=a)
    print(formu)
    if formu.is_valid():
        formu.save()
    return HttpResponse("foto guardada correctamente")


@login_required(login_url='login/')
def usuario(r):
    return render(r, "system/account/usuario.html")


@login_required(login_url='login/')
def guardaperfil(r):
    a = get_object_or_404(Users, pk=r.user.id)
    form = cueform(r.POST, instance=a)
    if form.is_valid():
        form.save()
        return HttpResponse("guardado")
    else:
        if r.is_ajax():
            ee = {}
            if form.errors:
                for i in form.errors:
                    e = form.errors[i]
                    ee[i] = str(e)

            return HttpResponseBadRequest(json.dumps(ee))


@login_required(login_url='login/')
def usuariosCompletado(r):
    return HttpResponse("ok registrado")


def seleccionar(size=1, chars=string.ascii_uppercase + string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))


def permisoPersonalizados(request):
    idpermisos = []
    array = []
    id = request.GET.get("id")
    modulo = modules.objects.values('id', 'description', 'father')
    todosPermisos = permission.objects.values('id', 'descripcion', 'idmodulo')
    moduloselect = det_user_permiso.objects.values('idpermiso__idmodulo', 'idpermiso__idmodulo__description') \
        .filter(iduser_id=request.GET.get("id"), status=True).annotate(contar=Count('idpermiso__idmodulo'))
    permisos = det_user_permiso.objects.values('idpermiso__id') \
        .filter(iduser_id=request.GET.get("id"), status=True)
    for fee in permisos:
        idpermisos.append(fee['idpermiso__id'])
    for foo in moduloselect:
        array.append(foo['idpermiso__idmodulo'])
    b = 'Actualizar Permiso'
    return render(request, 'system/profile/frm_profile.html',
                  {'btn': b, 'idprofile': id, 'modulo': modulo, 'moduloselec': array, 'permisos': idpermisos,
                   'mudulosimpr': moduloselect, 'todospermiso': todosPermisos})


def actualizarpermisoPersonalizado(request):
    idusuario = request.GET.get('idperfil')
    permisotempalte = request.GET.getlist('idpermiso')
    datopermiso = det_user_permiso.objects.values('idpermiso_id', 'status').filter(iduser_id=idusuario)
    listapermiso = {}
    for foo in datopermiso:
        listapermiso[foo['idpermiso_id']] = foo['status']
    for fee in permisotempalte:
        if int(fee) in listapermiso:
            if listapermiso[int(fee)] != True:
                actualizarTrue = det_user_permiso.objects.get(iduser_id=idusuario, idpermiso=int(fee))
                actualizarTrue.status = True
                actualizarTrue.save()
            del listapermiso[int(fee)]
        else:
            guardarDetPermiso = det_user_permiso()
            guardarDetPermiso.iduser_id = int(idusuario)
            guardarDetPermiso.idpermiso_id = int(fee)
            guardarDetPermiso.save()
    if listapermiso:
        for fii in listapermiso:
            actualizarTrue = det_user_permiso.objects.get(iduser_id=idusuario, idpermiso=int(fii))
            actualizarTrue.status = False
            actualizarTrue.save()
    return HttpResponse("Actualizado correctamente")


def enviarCorreo(form):
    print(form.cleaned_data["email"])
    send_mail(
        str(form.cleaned_data["nombre"]),
        str('Email: ' + form.cleaned_data["email"] + ' Teléfono: ' + str(form.cleaned_data["telefono"]) +
            form.cleaned_data["message"]),
        str(form.cleaned_data["email"]),
        ['mindisoft@gmail.com'],
        fail_silently=False,
    )
    send_mail(
        'Mindi le da la bienvenida!',
        'Gracias por contactarse con nosotros, lo atenderemos lo más pronto posible',
        'mindisoft@gmail.com',
        [form.cleaned_data["email"]],
        fail_silently=False,
    )
    return HttpResponse("enviado correctamente")


def indeContacto(r):
    print("llego");
    print(r.POST.get('email'))
    send_mail(
        'Mindi le da la bienvenida!',
        'Gracias por contactarse con nosotros, lo atenderemos lo más pronto posible',
        'mindisoft@gmail.com',
        [r.POST.get('email')],
        fail_silently=False,

    )
    success_url = reverse_lazy('guardarMensaje')
    return HttpResponse("sadas")


'''
class FormContacto(FormView):
    form_class = ''
    template_name = "contacto/contacto.html"
    success_url = reverse_lazy('guardarMensaje')
def contactoGuardar(r):
    contactoguarda=formcontacto(r.GET)
    if contactoguarda.is_valid():
       contactoguarda.save()
    return  HttpResponse("llego")


    contactoguarda=contacto()
    contactoguarda.nombre= r.GET.get("name")
    contactoguarda.email= r.GET.get("email")
    contactoguarda.telefono= r.GET.get("subject")
    contactoguarda.message= r.GET.get("message")
    contactoguarda.save()
def guardar_contacto(r):

    guardarContacto = contacto()
    guardarContacto.nombre = r.GET.get("name")
    guardarContacto.email = r.GET.get("email")
    guardarContacto.message = r.GET.get("message")
    guardarContacto.telefono = r.GET.get("subject")
    guardarContacto.save()
    return HttpResponse("ok registrado")

    return HttpResponse ("llego")
    contactoguarda=formcontacto(r.GET)
    if contactoguarda.is_valid():
       contactoguarda.save()
    return  HttpResponse("llego")


def contactos(r):
    formulariocontacto= formcontacto
    return render(r,"contacto/contacto.html",{'formulariocontacto':formulariocontacto})



    return render(r,"contacto/contacto.html")
'''


def permisoPersonalizados(request):
    idpermisos = []
    array = []
    id = request.GET.get("id")
    modulo = modules.objects.values('id', 'description', 'father')
    todosPermisos = permission.objects.values('id', 'descripcion', 'idmodulo')
    moduloselect = det_user_permiso.objects.values('idpermiso__idmodulo', 'idpermiso__idmodulo__description') \
        .filter(iduser_id=request.GET.get("id"), status=True).annotate(contar=Count('idpermiso__idmodulo'))
    permisos = det_user_permiso.objects.values('idpermiso__id') \
        .filter(iduser_id=request.GET.get("id"), status=True)
    for fee in permisos:
        idpermisos.append(fee['idpermiso__id'])
    for foo in moduloselect:
        array.append(foo['idpermiso__idmodulo'])
    b = 'Actualizar Permiso'
    return render(request, 'system/profile/frm_profile.html',
                  {'btn': b, 'idprofile': id, 'modulo': modulo, 'moduloselec': array, 'permisos': idpermisos,
                   'mudulosimpr': moduloselect, 'todospermiso': todosPermisos})


def actualizarpermisoPersonalizado(request):
    idusuario = request.GET.get('idperfil')
    permisotempalte = request.GET.getlist('idpermiso')
    datopermiso = det_user_permiso.objects.values('idpermiso_id', 'status').filter(iduser_id=idusuario)
    listapermiso = {}
    for foo in datopermiso:
        listapermiso[foo['idpermiso_id']] = foo['status']
    for fee in permisotempalte:
        if int(fee) in listapermiso:
            if listapermiso[int(fee)] != True:
                actualizarTrue = det_user_permiso.objects.get(iduser_id=idusuario, idpermiso=int(fee))
                actualizarTrue.status = True
                actualizarTrue.save()
            del listapermiso[int(fee)]
        else:
            guardarDetPermiso = det_user_permiso()
            guardarDetPermiso.iduser_id = int(idusuario)
            guardarDetPermiso.idpermiso_id = int(fee)
            guardarDetPermiso.save()
    if listapermiso:
        for fii in listapermiso:
            actualizarTrue = det_user_permiso.objects.get(iduser_id=idusuario, idpermiso=int(fii))
            actualizarTrue.status = False
            actualizarTrue.save()
    return HttpResponse("Actualizado correctamente")

    '''form=formcontacto(r.POST)
    if form.is_valid():
def contacto(r):
    form=formcontacto()


    return render(r,'system/contacto/contacto.html')
    '''  # if form.is_valid():
    # return HttpResponse ("llego")


def contactos(r):
    formulariocontacto = formcontacto
    return render(r, "contacto/contacto.html", {'formulariocontacto': formulariocontacto})
    '''form=formcontacto(r.POST)
    if form.is_valid():
        form.save()
        send_mail(
            str(form.cleaned_data["nombre"]),
            str('Email: '+form.cleaned_data["email"]+ ' Teléfono: '+str(form.cleaned_data["telefono"])+form.cleaned_data["message"]),
            str(form.cleaned_data["email"]),
            ['softemmarc@gmail.com'],
            fail_silently=False,
        )
        send_mail(
            'Mindi le da la bienvenida!',
            'Gracias por contactarse con nosotros, lo atenderemos lo más pronto posible',
            'softemmarc@gmail.com',
            [form.cleaned_data["email"]],
            fail_silently=False,
        )
        return HttpResponse("enviado correctamente")
    else:
        if r.is_ajax():
            ee = {}
            if form.errors:
                for i in form.errors:
                    e = form.errors[i]
                    ee[i] = str(e)

    def form_valid(self, form):
        form.save()
        enviarCorreo(form)
        return super(FormContacto, self).form_valid(form)

    def form_invalid(self, form):
        return error(form)

def guardarMensaje(r):
    return HttpResponse("Enviado Correctamente")
'''
