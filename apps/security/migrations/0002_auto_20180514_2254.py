# Generated by Django 2.0.5 on 2018-05-15 03:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('security', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='modules',
            name='order_mod',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='modules',
            name='order_submod',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
