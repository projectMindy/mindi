import json

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.serializers.json import DjangoJSONEncoder
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import TemplateView
from django.views.generic.base import ContextMixin, View
from django.views.generic.edit import FormView, UpdateView
from psycopg2.extras import NumericRange

from apps.security.core import error


class LazyEncoder(DjangoJSONEncoder):
    def default(self, obj):
        if isinstance(obj, NumericRange):
            obj = {'max': obj.upper, 'min': obj.lower}
            return obj
        return super(LazyEncoder, self).default(obj)


class ResponseView(ContextMixin, View):

    def l_queryset(self):
        pass

    def response(self, context):
        '''
            :return: HttpResponse 
        '''
        pass

    def response_json(self):
        '''
            :return: HttpResponse Json
        '''
        return HttpResponse(json.dumps(self.l_queryset(), cls=LazyEncoder))

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.response(context)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.response(context)


class ResponseLoginView(LoginRequiredMixin, ResponseView):
    login_url = reverse_lazy('login')
    '''
        :return: HttpResponse Login Mixin
    '''
    pass


class TemplateLoginRequired(LoginRequiredMixin, TemplateView):
    login_url = reverse_lazy('login')
    template_name = ''


class ListBase(TemplateLoginRequired):
    login_url = reverse_lazy('login')
    template_name = ""
    queryset = None

    def l_queryset(self):
        pass

    def get_context_data(self, **kwargs):
        context = super(ListBase, self).get_context_data(**kwargs)
        if self.queryset is None:
            context['object_list'] = self.l_queryset
        else:
            context['object_list'] = self.queryset
        return context


class SaveBase(LoginRequiredMixin, FormView):
    login_url = reverse_lazy('login')
    form_class = ""
    template_name = ""
    success_url = reverse_lazy('ok')
    form_class2 = None

    def get_context_data(self, **kwargs):
        context = super(SaveBase, self).get_context_data(**kwargs)
        context['name'] = 'Registrar'
        if self.form_class2 is not None:
            context['form2'] = [[self.form_class2, '']]
        return context

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        return error(form)


class UpdateBase(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('login')
    template_name = ""
    success_url = reverse_lazy('ok')
    form_class2 = None
    model2 = None
    queryset2 = None

    def l_queryset(self, p):
        pass

    def get_context_data(self, **kwargs):
        context = super(UpdateBase, self).get_context_data(**kwargs)
        context['name'] = 'Actualizar'
        context['pk'] = self.kwargs["pk"]
        if self.form_class2 is not None:
            a = self.l_queryset('')
            nf = []
            for i in a:
                ff = self.form_class2(instance=i)
                nf.append([ff, i.id])
            context['form2'] = nf
        return context

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        return error(form)
