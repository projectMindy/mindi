from django.contrib import admin


class BaseAdmin(admin.ModelAdmin):
    def get_form(self, request, obj=None, **kwargs):
        self.exclude = ['deleted_at', 'status']
        return super(BaseAdmin, self).get_form(request, obj, **kwargs)
