#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.shortcuts import render
from apps.security.models import Users_Core
from django.db.models import Max, Q
from .select import searchQ
from django.db import connection

def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
        ]

def datos_ajax(r):
    dat = r.GET.get('campo')
    tabla= r.GET.get('tabla')
    camp = eval(r.GET.get('list'))
    v = r.GET.get('values')
    template = r.GET.get('template')

    uu = eval(tabla)
    st = ''
    c = 0
    for i in camp:
        cad = "Q(" + i + "__icontains = dat)"
        if c == 0:
            st += cad
        else:
            st += "|"+cad
        c+=1
    s= eval(st)

    if v != "None":
        vv = ".values("+v+")"
    else:
        vv = ""

    pad = eval("uu.objects.filter(s)"+vv+"[:10]")

    modulo = {'lista':pad,'nroP':0}
    return render(r, template, modulo)

from django.views.generic import TemplateView
class data_search(TemplateView):
    template_name = ""
    model         = ''
    query         = ''
    camp          = ''
    values        = ''
    filter        = ''

    def get_context_data(self, **kwargs):
        context = super(data_search, self).get_context_data(**kwargs)
        tabla = self.model
        dat = self.request.GET.get('campo')

        if self.query != "None":
            select   = self.query["select"]
            join     = self.query["join"]
            w = self.query["where"]
            w += searchQ(select,self.query["exclude"], str(dat))
            w +="))"
            group_by = self.query["group_by"]
            if group_by:
               g = ' GROUP BY ' + group_by
            else:
                g = ''
            q = 'SELECT ' + select + ' FROM ' + join + '  WHERE ' + w + g
            pad = connection.cursor()
            pad.execute(q)
            pad = dictfetchall(pad)

        else:
            camp = self.camp
            v = self.values

            st = ''
            c = 0
            for i in camp:
                cad = "Q(" + i + "__icontains = dat)"
                if c == 0:
                    st += cad
                else:
                    st += "|" + cad
                c += 1
            if self.filter:
                s = st+ self.filter
            s = eval(st)

            if v != "None":
                vv = ".values(" + v + ")"
            else:
                vv = ""

            pad = eval("tabla.objects.filter(s)" + vv + "[:10]")

        context['lista'] = pad
        context['nroP'] = 0
        return context