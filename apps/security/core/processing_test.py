from django.db.models.aggregates import Count
from django.http import HttpResponseBadRequest

from apps.adm_security.models import Users
from apps.adm_test.models import History_test_Patient
from .core import edadActual, save_error
from apps.test_psyco.models import Answers, Dimensions, TestPsicologico, Results, Validacion_pf
import sys


def preProcesamiento(dixy, idtest):
    consulta = Answers.objects.values("value_marked", "answer__preguntas__dimensiones",
                                      "answer__alternativas__iddimension"). \
        filter(history=dixy.POST.get('history')).order_by("answer__preguntas__correlativo")
    idp = History_test_Patient.objects.filter(id=dixy.POST.get('history')).values("patient__id")[0]["patient__id"]
    dim = Dimensions.objects.values().filter(test=idtest)
    test = TestPsicologico.objects.values().filter(id=idtest)
    validation = Validacion_pf.objects.select_related("type_validation").filter(testpsicologico=idtest)
    processing(dixy, consulta, dim, test, validation, idp)


def processing(request, consulta, dimension, test, validation, idp):
    # validation_general(validation)
    save_range_diagnose(request, calculate_score(request, dimension, consulta, test, idp),
                        range_dimention_variable(request, test, idp), test, idp)


def validation_general(validation, **kwargs):
    def validation_formula(validation_type, padre=None):
        for validation_formulate in validation.filter(type_validation_id=validation_type, father=padre):
            try:
                if eval(validation_formulate['validacion']):
                    if validation_formulate['proceso'] != 'continuing':
                        return HttpResponseBadRequest("Error en la transacción")
                    validation_formula(validation_type, validation_formulate['id'])
            except:
                break

    def validation_tipo():
        for validation_type in validation.type_validation_id:
            validation_formula(validation_type)


def calculate_score(request, dimension, consulta, test, idp):
    dimension_formula = []
    dimension_temporal = []
    puntajedimension = {}

    def changing():
        dimension_formula.clear()
        dimension_formula.extend(dimension_temporal)
        dimension_temporal.clear()

    def formulate(formula, pi=None, puntajedimension=None, al=None):
        try:
            return eval(str(formula["formula"]))
        except:
            dimension_temporal.append(formula)
            pass

    def dimention_question():
        pi = For_global(request, Funtions.for_range, range(test.values('questions__id').count()), idp,
                        *consulta)
        changing()
        for formula in dimension_formula:
            puntajedimension[formula["id"]] = formulate(formula, pi.__dict__)

    def dimention_alternatives():
        al = {}
        for numeroAlternativa in test.values('questions__detallealtpreg__id', 'questions__detallealtpreg__puntaje'):
            try:
                al[numeroAlternativa['questions__detallealtpreg__id']] = \
                    int(consulta.values('value_marked').filter(
                        answer_id=numeroAlternativa['questions__detallealtpreg__id'])[0]['value_marked'])

            except (IndexError, ValueError) as err:
                al[numeroAlternativa['questions__detallealtpreg__id']] = 0
            except:
                save_error(idp, sys.exc_info())
                return HttpResponseBadRequest("Error en la transacción")
        changing()
        for form in dimension_formula:
            puntajedimension[form["id"]] = formulate(form, al=al)

    def dimention_dimention():
        changing()
        for formul in dimension_formula:
            puntajedimension[formul["id"]] = formulate(formul, puntajedimension=puntajedimension)

    funtion = {'question': dimention_question, 'alternatives': dimention_alternatives, 'dimention': dimention_dimention}
    dimension_temporal.extend(dimension)

    for correction in test[0]['level_correction'].split(','):
        funtion[correction]()
    return puntajedimension


def range_dimention_variable(request, test, idp):
    id_variable = []

    def variable(id_categoria, padre=None):
        date = test.filter(detallevartest__padre=padre, detallevartest__variables__categoria_id=id_categoria) \
            .values("detallevartest__variables__descripcion", "detallevartest__variables__rango"
                    , "detallevartest__variables__categoria__nombre", "detallevartest__id")
        resultado = getattr(Variable_test, date[0]['detallevartest__variables__categoria__nombre']) \
            (variable, date, idp)
        try:
            variable(id_categoria, padre=resultado["detallevartest__id"])
        except IndexError:
            return resultado["detallevartest__id"]
        except:
            save_error(idp, sys.exc_info())
            return HttpResponseBadRequest("Error en la transacción")

    for var in test.annotate(dcount=Count('detallevartest__variables__categoria_id')) \
            .values('detallevartest__variables__categoria_id'):
        if var['detallevartest__variables__categoria_id'] != 1:
            id_variable.append(variable(var['detallevartest__variables__categoria_id']))
        else:
            date = test.filter(detallevartest__variables__categoria_id=var['detallevartest__variables__categoria_id']) \
                .values("detallevartest__id")
            for foo in date:
                id_variable.append(foo["detallevartest__id"])
    # print(id_variable)
    return id_variable


def save_range_diagnose(request, score, rangedimention, test, idp):
    def save(value, diagonostico=None, time=None):
        guar = Results()
        guar.score = value
        guar.diagnostics_id = diagonostico
        guar.time = 12  # tambien falta determinar
        guar.history_id = int(request.POST.get('history'))
        guar.save()

        h = History_test_Patient.objects.get(pk=request.POST.get('history'))
        h.status_test = "Realizado"
        h.save()

    for key, value in score.items():
        notrange = True
        try:
            for data in test.filter(dimensions__id=key,
                                    dimensions__rangospercentiles__det_variables__in=rangedimention). \
                    values("dimensions__rangospercentiles__diagnostics_id",
                           "dimensions__rangospercentiles__puntuacion"). \
                    order_by("dimensions__rangospercentiles__puntuacion"):
                # print(data['dimensions__rangospercentiles__puntuacion'])
                # print(value)
                # print(key)
                if not data['dimensions__rangospercentiles__puntuacion'] <= value:
                    notrange = False
                    save(value, diagonostico=diagonostico)
                    break
                else:
                    diagonostico = data["dimensions__rangospercentiles__diagnostics_id"]
            if notrange:
                save(value, diagonostico)
        except:
            # ya esta comprobado
            save_error(idp, sys.exc_info())
            return HttpResponseBadRequest("Error en la transacción")
    pass


class Variable_test():
    __slots__ = 'idp', 'date'

    def age(self, date, idp):
        us = Users.objects.filter(id=idp).values()

        edad = edadActual(us[0]["birth_date"])
        for rangeage in date:
            if float(rangeage['detallevartest__variables__rango'].lower) <= edad and \
                    float(rangeage['detallevartest__variables__rango'].upper) > edad:
                return rangeage
        # falta crear un terminar proceso y mandar un mensaje
        return "La edad esta fuera del rango que permite el test"

    def sex(self, date, idp):
        us = Users.objects.filter(id=idp).values()
        for sex in date:
            if sex['detallevartest__variables__descripcion'] == us[0]["sex"]:
                return sex
        return "No tiene sexo o es un sexo desconocido"

    def generic(self, date, idp):
        return date[0]


class Funtions(object):
    def for_range(self, request, foo, idp, *warg):
        try:
            self.__dict__[foo + 1] = (int(warg[foo]["value_marked"]))
        except IndexError:
            self.__dict__[foo + 1] = 0
        except:
            save_error(idp, sys.exc_info())
            return HttpResponseBadRequest("Error en la transacción")


class For_global(Funtions):
    __slots__ = 'funtion', 'parameter'

    def __init__(self, request, function, parameter, idp, *warg, **kwargs):
        self.for_funtion(self, function, parameter, idp, *warg, **kwargs)

    def for_funtion(self, request, funtion, parameter, idp, *warg, **kwargs):
        for foo in parameter:
            funtion(self, request, foo, idp, *warg, **kwargs)
