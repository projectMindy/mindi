from django.http import HttpResponseBadRequest
from django.core.exceptions import ImproperlyConfigured
from django.views.generic import TemplateView
import json


# recoger los errores de los formularios y enviarlo al servidor
def error(f):
    print("this see you, because there bug")
    ee = {}
    for i in f.errors:
        e = f.errors[i]
        ee[i] = str(e)
    return HttpResponseBadRequest(json.dumps(ee))


def get_template(self, template_name):
    if self.template_name is None:
        raise ImproperlyConfigured(
            "TemplateResponseMixin requires either a definition of "
            "'template_name' or an implementation of 'get_template_names()'")
    else:
        if self.request.is_ajax():
            return [template_name]
        else:
            return [self.template_name]


class error400(TemplateView):
    template_name = '400.html'

    def get_template_names(self):
        return get_template(self, template_name="error/error400.html")


class error404(TemplateView):
    template_name = '404.html'

    def get_template_names(self):
        return get_template(self, template_name="error/error404.html")


class error500(TemplateView):
    template_name = '500.html'

    def get_template_names(self):
        return get_template(self,template_name = "error/error500.html")

