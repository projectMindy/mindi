from datetime import datetime

def format_date(f):
    b = '-' in f
    d = "%d/%m/%Y" if not b else "%Y-%m-%d"
    return datetime.strptime(f, d)