from django import forms
from ..models import Grado

class GradoForm(forms.ModelForm):
  class Meta():
    model = Grado
    fields = '__all__'
    exclude = ('status', 'deleted_at')