from django import forms
from ..models import Directives

class directivesForm(forms.ModelForm):
  class Meta():
    model = Directives
    fields = '__all__'
    exclude = ('status', 'deleted_at')