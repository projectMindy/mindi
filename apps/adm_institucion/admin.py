from django.contrib import admin
from .models import Institucion, Seccion, Grado, Classroom, NivelEducativo
from apps.security.core.admin import BaseAdmin

# Register your models here.
admin.site.register(Institucion, BaseAdmin)
admin.site.register(Seccion, BaseAdmin)
admin.site.register(Grado, BaseAdmin)
admin.site.register(Classroom, BaseAdmin)
admin.site.register(NivelEducativo, BaseAdmin)
