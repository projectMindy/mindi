from apps.security.core.join_list import validate_dict
from apps.adm_test.templatetags.tags import get_patient
from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse

from apps.security.core.crud import ListBase, SaveBase, UpdateBase, ResponseView
from ..forms.f_History_test_Patient import History_test_PatientForm
from ..models import History_test_Patient
from apps.test_psyco.models import Answers


class list_History_test_Patient(ListBase):
    template_name = 'History_test_Patient/list_History_test_Patient.html'

    def l_queryset(self):
        arg = {}
        if self.request.GET:
            arg = validate_dict(self.request.GET)
        arg['status'] = True

        return History_test_Patient.objects.filter(**arg).order_by('-id')


class AddHistoryTestPatient(SaveBase):
    form_class = History_test_PatientForm
    template_name = 'History_test_Patient/frm_History_test_Patient.html'


class update_History_test_Patient(UpdateBase):
    form_class = History_test_PatientForm
    template_name = 'History_test_Patient/frm_History_test_Patient.html'
    model = History_test_Patient


class list_Patient_json(ResponseView):
    def l_queryset(self):
        return get_patient(self.request.GET.get("idp"))

    def response(self, context):
        return self.response_json()


@login_required(login_url='login/')
def deleteHistory_test_Patient(em, id):
    dp = History_test_Patient.objects.get(pk=id)
    # dp.status = False
    dp.status_test = 'No Realizado'
    dp.save()
    Answers.objects.filter(history__id=id).delete()

    return HttpResponse('DATOS ELIMINADOS')
