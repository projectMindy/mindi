from apps.security.core.join_list import validate_dict
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import HttpResponse

from apps.adm_institucion.models import Classroom, Directives
from apps.security.core.crud import ListBase, SaveBase, UpdateBase
from ..forms.f_Test_Programed import Test_ProgramedForm
from ..models import Test_Programed, History_test_Patient


class ListTestProgramed(ListBase):
    template_name = 'Test_Programed/list_Test_Programed.html'

    def l_queryset(self):
        arg = {}
        if self.request.GET:
            arg = validate_dict(self.request.GET)
        arg['status'] = True
        return Test_Programed.objects.filter(**arg).order_by('-id')


class AddTestProgramed(SaveBase):
    form_class = Test_ProgramedForm
    template_name = 'Test_Programed/frm_Test_Programed.html'

    def get_context_data(self, **kwargs):
        context = super(AddTestProgramed, self).get_context_data(**kwargs)
        u = Classroom.__get__data__ie__(self, {})
        d = Directives.__get__data__directive__(self, {})
        l = u
        li = {}
        for i in d:
            a = str(i.occupation)
            if a in li.keys():
                li[a].append(i.directive)
            else:
                li[a] = [i.directive]

        nli = []
        i = 100
        for j in li.keys():
            i = i + 1
            nli.append({'id': i, 'name': j, 'directives': li[j]})
        context['dir'] = nli

        context['patient'] = l
        return context

    def form_valid(self, form):
        te = form.save(commit=False)
        # te.test_id = form.cleaned_data.get('test')
        te.save()
        for pa in form.cleaned_data.get('patient'):
            h = History_test_Patient(patient=pa, test_programed=te)
            h.save()

        # form.save_m2m()
        return HttpResponseRedirect(self.get_success_url())


class UpdateTestProgramed(UpdateBase):
    form_class = Test_ProgramedForm
    template_name = 'Test_Programed/frm_Test_Programed.html'
    model = Test_Programed

    def form_valid(self, form):
        te = form.save(commit=False)
        te.save()
        obj = History_test_Patient.objects
        queryset = obj.filter(test_programed=te, status=True)
        patients_data = []
        patients_request = form.cleaned_data['patient']
        for a in queryset:
            patients_data.append(a.patient)
            if not (a.patient in patients_request):
                queryset.filter(patient_id=a.patient_id).delete()

        for i in patients_request:
            if not (i in patients_data):
                obj.create(patient=i, test_programed=te)

        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(UpdateTestProgramed, self).get_context_data(**kwargs)
        u = Classroom.__get__data__ie__(self, {})
        d = Directives.__get__data__directive__(self, {})
        context['patient'] = u
        li = {}
        for i in d:
            a = str(i.occupation)
            if a in li.keys():
                li[a].append(i.directive)
            else:
                li[a] = [i.directive]

        nli = []
        i = 100
        for j in li.keys():
            i = i + 1
            nli.append({'id': i, 'name': j, 'directives': li[j]})
        context['dir'] = nli

        return context


@login_required(login_url='login/')
def deleteTestProgramed(em, id):
    dp = Test_Programed.objects.get(pk=id)
    dp.status = False
    dp.save()
    return HttpResponse('DATOS ELIMINADOS')
