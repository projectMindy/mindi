from datetime import datetime

from django.db import models

from apps.security.core.core import TimeStampedModel
from apps.test_psyco.models import TestPsicologico


# Create your models here.
class Program_Test(TimeStampedModel):
    description = models.CharField(max_length=250)
    start_date = models.DateField()
    end_date = models.DateField()
    programmer = models.ForeignKey('adm_security.Users', related_name='programmer_test', null=True, blank=True,
                                   on_delete=models.CASCADE)
    ie = models.ForeignKey('adm_institucion.Institucion', on_delete=models.CASCADE)
    test = models.ManyToManyField(TestPsicologico, through='Test_Programed', blank=True)

    def __str__(self):
        return self.description


class Test_Programed(TimeStampedModel):
    program_test = models.ForeignKey(Program_Test, on_delete=models.CASCADE)
    test = models.ForeignKey(TestPsicologico, on_delete=models.CASCADE)
    date = models.DateField()
    time = models.TimeField()
    patient = models.ManyToManyField('adm_security.Users', through='History_test_Patient', blank=True)

    def __str__(self):
        template = '{0.test}'
        return template.format(self)

    @property
    def is_past_due(self):
        # print(datetime.datetime.now() >= self.date)
        d = str(self.date) + " " + str(self.time)
        return datetime.now().strftime("%Y-%m-%d %H:%M:%S") > d


class History_test_Patient(TimeStampedModel):
    patient = models.ForeignKey('adm_security.Users', on_delete=models.CASCADE)
    test_programed = models.ForeignKey(Test_Programed, on_delete=models.CASCADE)
    date_aplication = models.DateTimeField(auto_now=True)
    status_test = models.CharField(default="No Realizado", max_length=20, choices=(
        ('Realizado', 'Realizado'),
        ('No Realizado', 'No Realizado'),
        ('Interrumpido', 'Interrumpido')
    ), blank=True)

    def __get__patient__(self, kwargs):
        context = {
            'status': True,
        }
        context.update(kwargs)
        return History_test_Patient.objects.filter(**context)

    def __test__programed__byusers__(self):
        p = Program_Test.objects.filter(status=True, ie=self.request.session['institucion']['id']).order_by('-id')
        # t = Test_Programed.objects.filter(status=True).order_by('-id')
        h = History_test_Patient.objects.filter(status=True).order_by('-id')
        # get test asignaded a user
        l = []
        for i in p:
            li = {'test_programed__program_test__id': i.id, 'patient': self.request.user.id}
            test = h.filter(**li)
            l.append(
                {'program': i,
                 'test': test
                 }
            )

        return (l)
