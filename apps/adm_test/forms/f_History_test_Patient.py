from django import forms
from ..models import History_test_Patient

class History_test_PatientForm(forms.ModelForm):
  class Meta():
    model = History_test_Patient
    fields = '__all__'
    exclude = ('status', 'deleted_at')
    labels ={
      'patient': 'Paciente',
      'status_test' : 'Estado',
    }
