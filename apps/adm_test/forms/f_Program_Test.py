from django import forms
from ..models import Program_Test

class Program_TestForm(forms.ModelForm):
  class Meta():
    model = Program_Test
    fields = '__all__'
    exclude = ('status', 'deleted_at','ie','programmer')
    widgets ={
      'start_date' : forms.DateInput(format=('%Y-%m-%d'), attrs={'type': 'date','placeholder': 'dd/mm/aaaa'}),
      'end_date': forms.DateInput(format=('%Y-%m-%d'), attrs={'type': 'date', 'placeholder': 'dd/mm/aaaa'}),
    }
    labels ={
      'description': 'Descripción',
      'start_date' : 'Fecha de Inicio',
      'end_date'   : 'Fecha de Término'
    }

